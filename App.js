import React from 'react';
import AppNavigation from './src/router/Router';
import { createSsrStore } from './src/redux';
import { Provider } from 'react-redux';


const initialState={
    backendUrl:"https://staging.bitzlato.com",

}
export const store = createSsrStore({ initialState, logout: () => console.log('logout') });

export default class Root extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <AppNavigation />
            </Provider>
        );
    }
}
