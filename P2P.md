[TOC]

# Common description

BitzlatoBotBackend provides RESTful API based on JSON (for time
Unix-time format is used).

All routes should be implemented for `/api/p2p` and `/api/bot`,
that's why for "common" routes prefix is used `prefix`. 
Separate routes (`GET /api/p2p/whoami` and `POST
/api/bot/whois`) should be used only for  concrete routes scheme 
and for describing them only actual URL should be used.

For object management following HTTP-methods are used: `GET`
(get object), `PUT` (update object), `DELETE` (delete object). 
For object creation API uses method `POST` for address of collection which will contains created object.
For non REST operations POST requests against address for that operation should be used  

Standard HTTP response codes are used:

* `200 OK` - for all successfully processed requests

* `401 Unauthorized` - authentication information is absent or incorrect. 
Access token is outdated and  authentication should be done again

* `403 Forbidden` - requested information is related to another user or access token do not corresponds this gateway.
  
* `500 Internal Server Error` - Something goes wrong. Should be no such errors if work is correct.

Also a few non-standard http response code are used for errors reporting.

* `461 Proposal Outdated` - 
Proposal is outdated, because it's time was up or someone had used it already

* `462 Not Enough Funds` - Not enough funds for operation 

* `463 Rate Was Changed` - rate was changed

* `464 Voucher Outdated` - Voucher outdated and can't be used

* `465 Unobvious Requirements` - not enough data, server can't define what user wants to do (for example for amount of voucher calculation) and data needs to be specified
  
* `466 Details Required` - can't start a trade, trades requisites needed for payment.
  
* `467 Internal Withdrawal` - Can't transfer to internal address
  
* `468 Not Enough Rating` -  Not enough user rating for operation

* `469 User Don't Accept Messages Without Transaction` - user don't accept messages without transaction

* `470 Already Have A Trade With A User` - 
  user already has active trade with current user. Response body should be in format: 
  ```{ tradeId: ... }```, where `tradeId` is id of not finished trade

* `471 Safety Wizard Required` - User should pass through safety wizard

* `472 Maintenance Enabled` - The system is in maintenance mode. Can't process request

* `473 Forbidden To Trade With Yourself` - Can't make trade based on own advert 

* `474 Already Done` - attempt to make an operation again which can be done only once

* `475 Service Shutdown` - Service is under work. There is no possibility to use it temporarily 

* `476 Advert Unavailable` - No possibility ti make new trade based on current advert

* `477 Advert Cannot Be Activated` - advert can't be activated, body should contain field `unactiveReason`
  with error code (check `GET /prefix/:userId/dsa/:advertId`)

All routes (except the  `Authentication` chapter) can be split into 2 parts:

* Routes with access with no authorization `/prefix/public/`

* Routes with  authorization `/prefix/:userId/`. Get info in user context.

Some of routes from `/prefix/public/` can have analogue
from `/prefix/:userId/` made with user context.

# Authentication

## GET /api/p2p/whoami

Getting information about the calling user if transmitted access token. 
Response format:

* `userId` - user identifier

* `name` - display name of user

## POST /api/bot/whois

Getting information about the calling user based 
on the `telegramId`. Request format:

    {
      telegramId: ...
    }

* `telegramId` - user identifier in the Telegram

Response format:

    {
      userId: ...
    }

* `userId` - user identifier

## GET /api/auth/whoami

Getting information about the calling user if transmitted access token. Response format:

    {
      userId: ...,
      name: ...
    }

* `userId` - user identifier

* `name` - display name of user


## POST /api/auth/access-token/

Getting access token. Request format:

    {
      regenerate: ...
    }

* `regenerate` - boolean is a sign that it is necessary to create a new token to replace the previous one. Otherwise the method should
 return an existing order if one already exists.

Response format:

    {
      accessToken: ...,
      expiryDate: ...
    }

* `accessToken` - access token

* `expiryDate` - access token expiration date

# User Settings

## GET /prefix/:userId/profile/

User profile description. Creates with first request. Response format:

    {
      name: ...,
      "licensingAgreementtAccepted": ...,
      "lang": "en",
      "currency": "USD",
      "cryptocurrency": "BCH",
      "startOfUseDate": 1525775076743,
      verification: {
        status: ...,
        url: ...
      },
      canCreateAdvert: {
        status: ...,
        reason: ...
      },
      serviceFee: ...,
      showTelegramName: ...,
      telegram: {
        id: ...,
        username: ...,
        first_name: ...,
        last_name: ...
      },
      greeting: ...,
      safeModeEnabled: ...,
      passSafetyWizard: ...,
      merged: ...
    }
    
* `userId` - User Id

* `publicName` - User Public Name

* `licensingAgreementtAccepted` - Boolean, user accepted licence agreement or not

* `lang` - language code, format `ISO 639-1`
  
* `currency` -  user preferred currency, can be `null`
  
* `cryptocurrency` - user preferred cryptocurrency, can be `null`

* `startOfUseDate` - Timestamp, date of start use wallet

* `verification` - verification status description

* `verification.status` - Boolean, is user verificated or not

* `verification.url` - link to verification page / verification status / detail information
  
* `canCreateAdvert` - user new ads creation description

* `canCreateAdvert.status` - Boolean, can user create new ads or not

* `canCreateAdvert.reason` - status code. explains the reason why user cant create new ads. possible values:
  
  - `not-enough-trades` - user did not do enough amount of deals
  
  - `blocked` - user banned (blocked by admin)
  
* `serviceFee` -   
  Service fee enabled for user.
  If user can't change own service fee status field should be set `null` 
  
* `showTelegramName` - Boolean, if true then users `telegram username` shows as  `publicName` 
  
* `telegram` - users `telegram` description

* `telegram.id` - users `id` in `telegram`. Currently equals to `userId`

* `telegram.name` - users login in `telegram`

* `telegram.first_name` - field `first_name` in user options in `telegram`

* `telegram.last_name` - `last_name` field in `telegram`    

* `greeting` - Greeting text which will be displayed in user info 

* `safeModeEnabled` - Boolean, is user safe mode on or off

* `passSafetyWizard` - Boolean, is user passed security test or not

* `suspicious` - Boolean, is user suspicious or not

* `merged` - Boolean, is user account merged or not

## PUT /prefix/:userId/profile/

Allows to change user options. Request format equals to Response format of request `GET /prefix/profile/:userId`. 
But all request parameters are optional, so you can send only changed parameters.

Following options cannot be changed by this request:

* `startOfUseDate`

* `verification`

* `canCreateAdvert`

Response format equals to Response format of request `GET /prefix/:userId/profile/`

## POST /api/p2p/:userId/profile/merge

Account merge. Currently available only with `telegram`.
Request format:

    {
      telegram: {
        id: ...,
        username: ...,
        first_name: ...,
        last_name: ...,
        photo_url: ...
      }
    }

Structure format `telegram` equals to field format `telegram` in Response `GET /prefix/:userId/profile/`,
but may have additional fields (`photo_url`). //!!!

Response format equals to Response format of request `GET /prefix/:userId/profile/`

## GET /prefix/:userId/profile/features

User features. Response format:

    [
      'Feature1',
      'Feature2',
      ...
    ]
    
i.e. list of features

## GET /prefix/:userId/profile/public-names/

Get list of possible public names, which could be chosen by user. Response format:

    [
      {
        publicName: ...
      },
      ...
    ]

* `publicName` - public name

## GET /prefix/:userId/profile/addresses/?cryptocurrency=:cryptocurrency

Get list of addresses for payment. Request format:    //!!!

* `cryptocurrency` - cryptocurrency code, optional

Response format:

    [
      {...},
      ...
    ]

Array description format equals to Response format `GET /prefix/wallets/:userId/:cryptocurrency/transfer-out/addresses/:id`       //!!!

## POST /prefix/:userId/profile/addresses/

Add new address into payment "addresses"        //!!!

    {
      id: ...,
      cryptocurrency: ...,
      description: ..., 
      address: ...
    }

* `cryptocurrency` - cryptocurrency code

* `description` - users address name

* `address` - address for payment   //!!!

Response format equals to Response format `GET /prefix/wallets/:userId/:cryptocurrency/transfer-out/addresses/:id`.

## GET /prefix/:userId/profile/addresses/:id 

    {
      id: ...,
      description: ...,
      address: ...
    }

* `id` - identificator id

* `description` - users address name

* `address` - address for payment 

## DELETE /prefix/:userId/profile/addresses/:id

Delete address from "addresses"

## GET /prefix/:userId/profile/requisites/?paymethod=:paymethod

Get list of chosen requisites for payment. //!!!

* `paymethod` - paymethod name, optional

## POST /prefix/:userId/profile/requisites/

Add requisite to favorite. Request format:

    {
      paymethod: ...,
      name: ...,
      details: ...
    }

* `paymethod` - paymethod id

* `name` - paymethod name 

* `details` - description, for example bank account

## GET /prefix/:userId/profile/requisites/:id

Get requisite details:

    {
      id: ...,
      paymethod: ...,
      name: ...,
      details: ...
    }

* `id` - unique id

* `paymethod` - paymethod id

* `name` - paymethod name 

* `details` - description, for example bank account


## DELETE /prefix/:userId/profile/requisites/:id

Delete requisite from "addresses"

## GET /prefix/:userId/profile/rate-sources/:cryptocurrency?currency=:currency

Rate source description for cryptocurrency `cryptocurrency`.
Response format: 

Response format:

    {
      id: ...,
      description: ...,
      url: ...,
      rate: ...
    }

* `id` - Rate source id

* `description` - Rate source description

* `url` - optional Rate source website link

* `rate` - current cruptocurrency-currency course (if `currency` set)

## POST /prefix/:userId/profile/rate-sources/:cryptocurrency

Calculation absolute or percent course from user profile rate source.
Request format:

    {
      currency: ...,
      value: ...,
      percent: ...
    }
    
* `currency` - Сalculation currency

* `value` - absolute value

* `percent` - percent value

Request accepts only one of `value` or `percent` field.

Response format equals to request format. But all fields have to be filled.
If `value` filled in request, then `percent` should be in response and vice-versa.

## POST /prefix/:userId/profile/rate-sources/

Set rate source:

    {
      currency: ...,
      cryptocurrency: ...,
      url: ...
    }
    
* `currency` - currency code

* `cryptocurrency` - cryptocurrency code

* `url` - rate source url

## GET /prefix/:userId/profile/referral-links?cryptocurrency=:cryptocurrency

Get list of referal links for user `userId`.

* `cryptocurrency` - optional (for one-currency bot requests)

Response format:

    {
      fee: ...,
      links: [
        {
          type: ...,
          url: ...
        },
        ...
      ]
    }

* `fee` - percent from deal, which goes for referal

* `links` - array with referal links

* `links.type` - referal link type. currently available values: `telegram`, `web`

* `links.url` - referal link


# Alerts

`alerts` - сигналы для пользователя, сообщающие о каких-либо проблемах, о предстоящих событиях и т.п.

## GET /prefix/:userId/alerts/**

Получение списка сигналов для пользователя, сообщающих о каких-либо проблемах, о предстоящих событиях и т.п.

Формат ответа:

    [
      {
        type: ...,
        domen: ...
        code: ...,
        data: ...
      },
      ...
    ]


* `type` - тип сигнала, возможные значения: `info`, `warning`, `error`

* `domain` - домен сигнала, произвольная строка (например, `profile`), которая может содержать в себе параметризованные данные, например, `adverts/BTC` - относиться к объявлениям для контексте `BTC` 

* `code` - код сигнала

* `data` - произвольные дополнительные данные, зависящие от кода сигнала

## GET /prefix/:userId/alerts/adverts/

Получение `alerts` для домена `adverts` для всех криптовалют.

## GET /prefix/:userId/alerts/adverts/:cryptocurrency

Получение `alerts` для домена `adverts/:cryptocurrency`.

# Публичная информация о пользователях

## GET /prefix/public/userinfo/:publicName

Получение открытой информации о пользователе системы:

* `publicName` - публичные имя пользователя, о котором запрашиваются
  данные

Формат ответа:

    {
      name: ...,
      greeting: ...,
      startOfUseDate: ...,
      verification: ...,
      rating: ..., 
      feedbacks: [
         {
           type: ...,
           count: ...
         },
         ...
      ],
      lastActivity: ...,
      dealStats: [
          {
            cryptocurrency: ...,
            totalCount: ...,
            totalAmount: ...,
            activeDeals: ...,
            canceledDeals: ...,
            defeatInDisputes: ...
          }
      ],
      suspicious: ...
    }

* `name` - публичное имя

* `greeting` - приветсвие пользователя

* `startOfUseDate` - дата начала пользования кошельком 

* `verification` - тип потверждения личности, в данный момент
  может быть: `none` и `documents`, в дальнейшем возможно расширения
  возможных типов верификации
  
* `rating` - рейтинг 

* `feedbacks` - статистика по оценка пользователей в процессе торговли

* `feedbacks.type` - типа оценки

* `feedbacks.count` - колличество оценок

* `lastActivity` - время последней активности в системе

* `chatAvailable` - признак возможности 

* `suspicious` - признак подозрительного пользвателя

* `dealStats` - статистика по совершенным сделками по всем криптовалютам пользователя

* `dealStats.cryptocurrency` - код криптовалюты

* `dealStats.totalAmout` - общее количество совершённых сделок в криптовалюте

* `dealStats.totalCount` - сумма всех совершённых сделок в криптовалюте

* `dealsStat.activeDeals` - количество активных сделок

* `dealsStats.canceledDeals` - количество отменённых сделок

* `dealsStats.defeatInDisputes` - количество поражений в диспутах

## GET /prefix/:userId/userinfo/:publicName 

Получение открытой информации о пользователе системы

* `userId` - идентификатор пользователя, для которого запрашиваются данные

* `publicName` - публичные имя пользователя, о котором запрашиваются
  данные
  
Формат ответа расширяет формат ответа маршрута `GET /prefix/public/userinfo/:publicName`
c помощью следующих полей:

* `blocked` - булев признак того, что пользователь `userId`
  заблокировал пользоваля `publicName`
  
* `chatAvailable` - булев признак того, что можно отправить сообщение пользователю

## POST /prefix/:userId/userinfo/:publicName/transfer

Позволяет совершить прямой перевод средств от пользователя `userId`
пользователю `publicName`

    {
      cryptocurrency: ...,
      amount: ...
    }
    
* `cryptocurrency` - код криптовалюты

* `amount` - количество средств для перевода


## POST /prefix/:userId/userinfo/:publicName/block

Позволяет заблокировать или разблокировать пользователя `publicName`
для пользователя `userId`.

Формат запроса:

    {
      block: ...
    }
    
* `block` - булев флаг: `true` - заблокировать, `false` - разблокировать


# Communication

## GET /prefix/:userId/userinfo/:publicName/chat/?limit=:limit&before=:before

Getting a list of messages as part of communication with the user. Message must be sorted by date created, first later posts

* `limit` - limit on the number of entries returned

* `before` - optional limit on the date the message was created, need to return messages, creation before the specified date

Response format:

    {
      data: [
        {
          id: ...,
          type: ...,
          created: ...,
          message: ...,
        },
        ...
      ],
      total: ...
    }

* `data` - list of messages in the same format as the response of route `GET /prefix/:userId/userinfo/:publicName/chat/:id`

* `total` - total amount of chat message

## POST /prefix/:userId/userinfo/:publicName/chat/

Allows send a message to the user. Request format:

    {
      message: ...
    }
    
* `message` - Message text

The response format is the same as the route response format. `GET /prefix/:userId/userinfo/:publicName/chat/:id`.

## GET /prefix/:userId/userinfo/:publicName/chat/:id

Getting chat message description by `id`. Response format:

    {
      id: ...,
      type: ...,
      created: ...,
      message: ...
    }

* `id` - message id

* `type` - message type, possible values: `in` (incoming) or `out` (outgoing)

* `created` -  date of sending message

* `message` - Message text

## GET /prefix/:userId/trade/:tradeId/chat/?limit=:limit&before=:before

Getting a list of messages as part of communication with the user. Message must be sorted by date created, first later posts

* `limit` - limit on the number of entries returned

* `before` - optional limit on the date the message was created, need to return messages, creation before the specified date. 


Response format:

    {
      data: [
        {
          id: ...,
          type: ...,
          created: ...,
          message: ...,
          file: {
             ...
          }
        },
        ...
      ],
      total: ...
    }

* `data` - list of messages in the same format as the response of route `GET /prefix/:userId/trade/:tradeId/chat/:id`

* `total` - total amount of chat messages
## POST /prefix/:userId/trade/:tradeId/chat/

Allows send a message to a transaction partner. Request format:

    {
      message: ...
    }
    
* `message` - Message text

The response format is the same as the route response format `GET /prefix/:userId/trade/:tradeId/chat/:id`

## POST /prefix/:userId/trade/:tradeId/chat/sendfile

Allows send a file to a transaction partner. Request format:
`multipart/form-data`.


Request parameters:

* `mime_type` - MIME-type file (option)

* `name` - file name (option)

* `telegram_file_id` - file id in `telegram` (option)

* `file` - file description

The response format is the same as the route response format.
`GET /prefix/:userId/trade/:tradeId/chat/:id`.


## GET /prefix/:userId/trade/:tradeId/chat/:id

Getting chat message description by `id`. Response format:

    {
      id: ...,
      type: ...,
      created: ...,
      message: ...,
      file: {
        title: ..,
            url: ...,
        telegramFileId: ...
      }
    }

* `id` - message id

* `type` - message type, possible values: `in` (incoming) or `out` (outgoing)

* `created` - date of sending message

* `message` - Message text

* `file.title` - file name

* `file.url` - url to download the file (must be available without access checking, but with this there is a "unusual" name, so you can't just crack it)

* `file.telegramFileId` - file id in `telegram` (if the file was uploaded by `telegram`)


## GET /prefix/:userId/trade/:tradeId/dispute/admin-chat/?limit=:limit&before=:before

Getting a list of messages as part of communication with the user. Message must be sorted by date created, first later posts

* `limit` - limit on the number of entries returned

* `before` - optional limit on the date the message was created, need to return messages, creation before the specified date 


Response format:

    {
      data: [
        {
          id: ...,
          type: ...,
          created: ...,
          message: ...,
          file: {
            title: ..
            url: ...,
            telegramFileId: ...
          }
        },
        ...
      ],
      total: ...
    }

* `data` - list of messages in the same format as the response of route
  `GET /prefix/:userId/trade/:tradeId/dispute/admin-chat/:id`

* `total` - total amount of chat messages

## POST /prefix/:userId/trade/:tradeId/dispute/admin-chat/

Allows send a message to the admin in the dispute on transaction. Request format:

    {
      message: ...
    }
    
* `message` - Message text

The response format is the same as the route response format.
`GET /prefix/:userId/trade/:tradeId/dispute/admin-chat/:id`.

## POST /prefix/:userId/trade/:tradeId/dispute/admin-chat/sendfile

Allows send a message to the admin in the dispute on transaction. Request format `multipart/form-data`.


Request parameters:

* `mime_type` - MIME-type file (option)

* `name` - file name (option)

* `telegram_file_id` - file id in `telegram` (option)

* `file` - file description

The response format is the same as the route response format.
`GET /prefix/:userId/trade/:tradeId/dispute/admin-chat/:id`.

## GET /prefix/:userId/trade/:tradeId/dispute/admin-chat/:id

Getting chat description with administrator as part of transaction by `id`. Response format:
    {
      id: ...,
      type: ...,
      created: ...,
      message: ...,
      file: {
        title: ...,
        url: ...,
        telegramFileId: ...
      }
    }

* `id` - message id

* `type` - message type, possible values: `in` (incoming) or `out` (outgoing)

* `created` - date of sending message

* `message` - Message text (null, if the field is passed `file`)

* `file` - file description (null, if the field is passed `message`)

* `file.title` - file name

* `file.url` - url to download the file (must be available without access checking, but with this there is a "unusual" name, so you can't just crack it)

* `file.telegramFileId` - file id in `telegram` (if the file was uploaded by `telegram`)



## GET /prefix/:userId/userinfo/:publicName/chat/?limit=:limit&before=:before

Получение списка сообщений в рамках общения с пользователем. Сообщение должны быть отсортированы по дате создания в
порядке убывания (т.е. сначал полее поздние сообщения). 

* `limit` - ограничение на количество возвращаемых записей

* `before` - опциональное ограничение по дате создания сообщения, необходимо
  отдавать сообщения, создание до указанной даты. 

Формат ответа:

    {
      data: [
        {
          id: ...,
          type: ...,
          created: ...,
          message: ...,
        },
        ...
      ],
      total: ...
    }

* `data` - массив сообщений в формате, совпадающем с форматом ответа
  маршрута `GET /prefix/:userId/userinfo/:publicName/chat/:id`

* `total` - общее колличество сообщений в чате

## POST /prefix/:userId/userinfo/:publicName/chat/

Позволяет отправить сообщение пользователю. Формат запроса:

    {
      message: ...
    }
    
* `message` - текст сообщения

Формат ответа совпадает с форматом ответа маршрута `GET /prefix/:userId/userinfo/:publicName/chat/:id`.

## GET /prefix/:userId/userinfo/:publicName/chat/:id

Получение описание сообщения чата по `id`. Формат ответа:

    {
      id: ...,
      type: ...,
      created: ...,
      message: ...
    }

* `id` - идентификатор сообщения

* `type` - тип сообщения, возможные значения: `in` (входящее) или `out` (исходящее)

* `created` - дата отправки сообщения

* `message` - текст сообщения

## GET /prefix/:userId/trade/:tradeId/chat/?limit=:limit&before=:before

Получение списка предыдущих сообщений в рамках общения с пользователем
в рамках сделки. Сообщение должны быть отсортированы по дате создания
в порядке убывания (т.е. сначал полее поздние сообщения).

* `limit` - ограничение на количество возвращаемых записей

* `before` - опциональное ограничение по дате создания сообщения, необходимо
  отдавать сообщения, создание до указанной даты. 


Формат ответа:

    {
      data: [
        {
          id: ...,
          type: ...,
          created: ...,
          message: ...,
          file: {
             ...
          }
        },
        ...
      ],
      total: ...
    }

* `data` - массив сообщений в формате, совпадющем с форматом ответа `GET /prefix/:userId/trade/:tradeId/chat/:id`

* `total` - общее колличество сообщений в чате

## POST /prefix/:userId/trade/:tradeId/chat/

Позволяет отправить сообщение партнёру по сделке. Формат запроса:

    {
      message: ...
    }
    
* `message` - текст сообщения

Формат ответа совпадает с форматом ответа `GET /prefix/:userId/trade/:tradeId/chat/:id`

## POST /prefix/:userId/trade/:tradeId/chat/sendfile

Позволяет отправить файл партнёру по сделке. Запрос в формате
`multipart/form-data`.

Параметры запроса:

* `mime_type` - MIME-тип файл (опционально)

* `name` - имя файла (опционально)

* `telegram_file_id` - идентификатор файла в `telegram` (опционально)

* `file` - файл

Формат ответа совпадает с форматом ответа маршрута
`GET /prefix/:userId/trade/:tradeId/chat/:id`.


## GET /prefix/:userId/trade/:tradeId/chat/:id

Получение описание сообщения чата сделки по `id`. Формат ответа:

    {
      id: ...,
      type: ...,
      created: ...,
      message: ...,
      file: {
        title: ..,
            url: ...,
        telegramFileId: ...
      }
    }

* `id` - идентификатор сообщения

* `type` - тип сообщения, возможные значения: `in` (входящее) или `out` (исходящее)

* `created` - дата отправки сообщения

* `message` - текст сообщения

* `file.title` - имя файл

* `file.url` - url для скачивания файла (должен быть доступен без проверки доступа, но при это иметь "диковинное" имя, что бы нельзя было его просто подобрать)

* `file.telegramFileId` - идентификатор файла в `telegram` (если файл был загружен через `telegram`)


## GET /prefix/:userId/trade/:tradeId/dispute/admin-chat/?limit=:limit&before=:before

Получение списка предыдущих сообщений в рамках общения с админом в
рамках сделки. Сообщение должны быть отсортированы по дате создания в
порядке убывания (т.е. сначал полее поздние сообщения).

* `limit` - ограничение на количество возвращаемых записей

* `before` - опциональное ограничение по дате создания сообщения, необходимо
  отдавать сообщения, создание до указанной даты. 


Формат ответа:

    {
      data: [
        {
          id: ...,
          type: ...,
          created: ...,
          message: ...,
          file: {
            title: ..
            url: ...,
            telegramFileId: ...
          }
        },
        ...
      ],
      total: ...
    }

* `data` - массив сообщений в формате, совпадающем с форматом ответа
  `GET /prefix/:userId/trade/:tradeId/dispute/admin-chat/:id`

* `total` - общее колличество сообщений в чате

## POST /prefix/:userId/trade/:tradeId/dispute/admin-chat/

Позволяет отправить сообщение админу в рамках диспута по
сделке. Формат запроса:

    {
      message: ...
    }
    
* `message` - текст сообщения

Формат ответа совпадает с форматом ответа маршрута
`GET /prefix/:userId/trade/:tradeId/dispute/admin-chat/:id`.

## POST /prefix/:userId/trade/:tradeId/dispute/admin-chat/sendfile

Позволяет отправить файл админу в рамках диспута по
сделке. Запрос в формате `multipart/form-data`.

Параметры запроса:

* `mime_type` - MIME-тип файл (опционально)

* `name` - имя файла (опционально)

* `telegram_file_id` - идентификатор файла в `telegram` (опционально)

* `file` - файл

Формат ответа совпадает с форматом ответа маршрута
`GET /prefix/:userId/trade/:tradeId/dispute/admin-chat/:id`.

## GET /prefix/:userId/trade/:tradeId/dispute/admin-chat/:id

Получение описание сообщения чата с админом в рамках сделки по
`id`. Формат ответа:

    {
      id: ...,
      type: ...,
      created: ...,
      message: ...,
      file: {
        title: ...,
        url: ...,
        telegramFileId: ...
      }
    }

* `id` - идентификатор сообщения

* `type` - тип сообщения, возможные значения: `in` (входящее) или `out` (исходящее)

* `created` - дата отправки сообщение

* `message` - текст сообщения (null, если передано поле `file`)

* `file` - описание файл (null, если передано поле `message`)

* `file.title` - имя файл

* `file.url` - url для скачивания файла (должен быть доступен без проверки доступа, но при это иметь "диковинное" имя, что бы нельзя было его просто подобрать)

* `file.telegramFileId` - идентификатор файла в `telegram` (если файл был загружен через `telegram`)

# Кошельки

## GET /prefix/:userId/wallets/

Получение списка кошельков пользователя. Формат ответа:

    [
      {
        cryptocurrency: ...
      }
    ]
    
* `cryptocurrency` - код криптовалюыт

## POST /prefix/:userId/wallets/

Создание нового кошелька. Формат запроса:

    {
      cryptocurrency: ...
    }

* `cryptocurrency` - код криптовалюты

Формат ответа совпадает с форматом ответа маршрута `GET /prefix/wallets/:userId/:cryptocurrency`.

## GET /prefix/:userId/wallets/:cryptocurrency?currency=:currency

Получение описания кошелька пользователя.
Или создание кошелька, если его нет.

* `userId` - идентификатор пользователя

* `cryptocurrency` - код криптовалюты

* `currency` - код валюты, для которой необходимо дать оценку размеру кошелька (на основе текущего курса)

Формат ответа:

    {
      cryptocurrency: ...,
      balance: ...,
      holdBalance: ...
      address: ..,
      worth: {
        currency: ...,
        value: ...
      },
      createdAt: ...,
    }
    
* `cryptocurrency` - код криптовалюты

* `balance` - сумма на балансе кошелька

* `holdBalance` - заблокированная сумма

* `address` - адрес кошелька для ввода средства, может отсутствовать
  (или быть `null`) если ещё не существует

* `worth` - оценочная стоимость в выбранной в настройках валюте на
  основе выбранного источника курса.

* `worth.currency` - код валюты 

* `worth.value` - стоимость в данной валюте

* `createdAt` - дата создания кошелька пользователя

## DELETE /prefix/:userId/wallets/:cryptocurrency 

Удаление кошелька. Операция выполняется успешно только в случае
отсутствия средства на кошельке.

В случае успешного удаления кошелька, сервер вернет 200 статус.
Если баланс кошелька не 0. Сервер вернет -> "Can't delete the balance is not 0", 423 статус.

## GET /prefix/:userId/wallets/:cryptocurrency/stats

Статистика использования кошелька. Формат ответа: 

    {
        dealsCount: ...,
        totalAmount: ...,
        invited: ...,
        earned: ...,
        rating: ...,
        feedbacks: [
           {
             type: ...,
             count: ...
           },
           ...
        ],
    }


* `dealsCount` - общее количество сделок

* `totalAmount` - сумма всех сделок

* `invited` - количество приглашённых пользователей

* `earned` - заработано (на приглашённых пользователях)

* `rating` - общий рейтинг

* `feedback` - обратная связь (оценка другими пользователями)

* `feedbacks.type` - типа оценки, в данный момент возможны значения: `thumb-up` и `thumb-down`

* `feedbacks.count` - колличество оценок

## GET /prefix/:userId/wallets/:cryptocurrency/withdrawal

Данные о возможности вывода средст с кошелька:

    {
      min: ...,
      available: ...,
      fee: ...,
      minBalance: ...
      balance: ...
    }

* `min` - минимальная сумма вывода

* `available` - сумма доступная для вывода

* `fee` - размер коммисси при выводе

* `minBalance` - минимальный баланс, который должен быть на кошельке для возможности вывода (`min + fee`)

* `balance` - текущий баланс

## POST /prefix/:userId/wallets/:cryptocurrency/withdrawal

Перевод средств на внешний кошелёк:

    {
      address: ...,
      amount: ...
    }
    
* `address` - адрес внешнего кошелька

* `amount` - количество средств для перевода

Формат ответа совпадает с форматом ответа маршрута `GET /prefix/:userId/transactions/:txid`

## POST /prefix/:userId/wallets/generate-address 

Генерация адреса для ввода средств. Формат запроса:

    {
      cryptocurrency: ...
    }

* `cryptocurrency` - код криптовалюты

# User Adverts

## GET /prefix/:userId/dsa/status

Returns information about the trade status of the user for each cryptocurrency. Response format:

    {
       BTC: ...,
       ETH: ...,
       DOGE: ...,
       ...
    }
    
The keys are cryptocurrency codes, and the values are boolean trading statuses

## PUT /prefix/:userId/dsa/status

Allows you to change the status of trading for one or more cryptocurrencies. The request format is the same as the response format of the
route `GET /prefix/:userId/dsa/status`, but all fields (cryptocurrency) are optional and only the actual transmitted data should be processed.

The format of the answer will fully match the format of the response route `GET /prefix/:userId/dsa/status`.

## GET /prefix/:userId/dsa/all?cryptocurrency=:cryptocurrency

Getting a list of ads for the user.

* `cryptocurrency` - if specified, it is necessary to return only the purchase / sale ads for the specified cryptocurrency

Response format:

    [
      {...},
      ...
    ]

The description format of the array element corresponds to the route response format
`GET /prefix/:userId/dsa/:advertId`

## POST /prefix/:userId/dsa/

Create new ad. The request format is the same as the response format of the route
`GET / prefix / dsa /: userId /: advertId` is following
exceptions:

* Field is not passed `id`

* The `rate.percent` field must be filled, either
   `rate.value`

Response format: `id: Int` - Created ad Id

Optional fields: `terms`, `details`.

The `status` field is not used during creation.  Ad created in
status `active`.

## GET /prefix/:userId/dsa/:advertId

Ad Description:

    {
      id: ...,
      userId: ...,
      type: ...,
      cryptocurrency: ...,
      paymethod: ...,
      ratePercent: ...,
      rateValue: ...
      minAmount: ...,
      maxAmount: ...,
      terms: ...,
      details: ...,
      status: ...,
      unactiveReason: ...,
      deepLinkCode: ...,
      links: [
        {
          type: ...,
          url: ...
        },
        ...
      ]
    }

* `id` - ad identifier

* `type` - ad type, possible options: `selling` or `purchase`
  
* `cryptocurrency` - cryptocurrency for which the ad is created

* `currency` - fiat currency for which the ad is created

* `paymethod` - payment method identifier

* `ratePercent` - the percentage used to determine the actual
   rate based on the current rate in the selected, for the currency used,
   the source (`GET /prefix/refs/rate-sources`).
  
* `rateValue` - immediate value of the cryptocurrency rate. If the `rate.percent` field is filled, it is calculated based on this field.

* `minAmount` - minimum amount of a potential transaction

* `maxAmount` - maximum amount of a potential transaction

* `terms` open part of the announcement

* `details` closed part of the announcement

* `status` - ad status, possible values: `active`, `pause`.

* `unactiveReason` - reason why ads are inactive:

    - `manual` - disabled by user

    - `autocancel` - disabled after auto cancel transaction

    - `not_enough_funds` - not enough funds in the user account
    
    - `ban` - ad blocked

    - `user_ban` - the user is blocked
    
    - `20_percent_diff`

* `deepLinkCode` - code to generate a link to open an ad


* `links` - array `deeplink` references

* `links.type` - link type, possible values: `telegram`, `web`

* `links.url` - link


## PUT /prefix/:userId/dsa/:advertId 

Edit ad description. The request format matches the format of the route `GET / prefix /: userId / dsa /: advertId` is followed by
exceptions:

* All fields are optional. Only actually processed
   passed parameters.
  
* Simultaneous indication of the fields `rate.percent` and` rate.value` are not allowed.

## DELETE /prefix/:userId/dsa/:advertId 

Delete ad.

## GET /prefix/:userId/dsa/paymethods/:type/:currency/:cryptocurrency/

List of payment options when creating ads.

* `type` - type of ad,` selling` or `purchase`

* `currency` - currency code
  
* `cryptocurrency` - cryptocurrency code

The response format matches the response format of the route `GET / prefix / public / refs / paymethods`.
  
  
# Объявления в системе

## GET /prefix/public/exchange/paymethods/?...

Получение описание методов оплаты, для которых есть предожения,
удовлетворяющие параметрам фильтрации.

Параметры запроса:

* `type` - тип предложения, `selling` или `purchase`

* `currency` - код валюты, используемой в методе оплаты

* `cryptocurrency` - код криптовалюты предложения

* `isOwnerActive` - признак того, что создатель объявления был недавно активен. Проверяется на равенство `true`.

* `isOwnerVerificated` - признак того, что создатель объявления прошёл проверку. Проверяется на равенство `true`.

* `limit` - ограничение на количество возвращаемых записей

* `skip` - количество записей, которые необходимо пропустить


Формат ответа:

    {
      data: [
        {
          id: ...,
          description: ...,
          rate: ...,
          count: ...
        },
        ...
      ],
      total: ...
    }

* `data.id` - идентификатор метода оплаты

* `data.description` - описание метода оплаты (например, `Сбербанк`)

* `data.rate` - текущий "лучший" курс обмена

* `data.count` - колличество предложений для данного метода оплаты

* `total` - общее колличество методов оплаты, соответствующих
  параметрам фильтрации
  
## GET /prefix/:userId/exchange/paymethods/?type=:type&currency=:currency&cryptocurrency=:cryptocurrency&limit=:limit&skip=:skip

Получение описание методов оплаты, для которых есть предожения,
удовлетворяющие параметрам фильтрации с учётом идентификатора
пользователя `userId` для исключение объявления "забаненных"
пользователей.

Параметры фильтра и формат ответа совпадают с маршрутом
`GET /prefix/public/exchange/paymethods/?type=:type&currency=:currency&cryptocurrency=:cryptocurrency&limit=:limit&skip=:skip`
  
## GET /prefix/public/exchange/dsa/?...

Получение списка предложений других пользователей согласно параметрам фильтрации.

Параметры запроса (всё параметры опциональны):

* `paymethod` - список идентификаторов методов оплаты, разделённых запятой

* `cryptocurrency` - код криптовалюты предложения

* `currency` - код валюты предложения

* `type` - тип предложения, `selling` или `purchase`

* `amount` - размер сделки в валюте (если передан `currency`)

* `isOwnerActive` - признак того, что создатель объявления был недавно активен. Проверяется на равенство `true`.

* `isOwnerVerificated` - признак того, что создатель объявления прошёл проверку. Проверяется на равенство `true`.

* `limit` - ограничение на количество возвращаемых записей

* `skip` - количество записей, которые необходимо пропустить

Формат ответа:

    {
      data: [
        {
          id: ...,
          type: ...,
          cryptocurrency: ...,
          currency: ...,
          paymethod: {
             id: ...,
             name: ...,
          },
          rate: ...,
          min: ...,
          max: ...,
          owner: ...,
          ownerLastActivity: ...
        },
        ...
      ],
      total: ...
    }

* `data.id` - идентификатор предложения

* `data.type` - типа объявления, `purshase` (автор покупает) или `selling` (автор продаёт)

* `data.currency` - используемая валюта

* `data.cryptocurrency` - используемая криптовалюта

* `data.paymethod` - описание метода оплаты

* `data.paymethod.id` - идентификатор метода оплаты

* `data.paymethod.name` - название метода оплаты

* `data.rate` - курс обмена в предожении

* `data.min` - минимальный размер сделки в валюте

* `data.max` - максимальный размер сделки в валюте

* `data.owner` - публичное имя автора предложения

* `data.ownerLastActivity` - время последней активности автора объявления

## GET /prefix/:userId/exchange/dsa/?...

Получение списка предложений согласно параметрам фильтрации с учётом
идентификатора пользователя `userId` для исключение объявления
"забаненных" пользователей.

Параметры фильтра и формат ответа совпадают с маршрутом `GET /prefix/public/exchange/dsa/?...`

## GET /prefix/public/exchange/dsa/:advertId

Получение публичного описания предложения. Формат ответа:

    {
      id: ...,
      available: ...,
      type: ...,
      cryptocurrency: {
        code: ...,
        min: ...,
        max: ...
      },
      paymethod: ...,
      rate: ...
      min: ...,
      max: ...,
      terms: ...,
      owner: ...,
      deepLinkCode: ...
    }

* `id` - идентификатор предложения

* `available` - булев признак того, что на основе данного объявления можно создать сделку

* `type` - тип предложения, `selling` или `purchase`

* `cryptocurrency` - описание криптовалюты объявления

* `cryptocurrency.code` - код криптовалюты

* `cryptocurrency.min` - минимальный размер сделки в криптовалюте

* `cryptocurrency.max` - максимальный размер сделки в криптовалюте

* `rate` - курс обмена в предожении

* `min` - минимальный размер сделки в валюте

* `max` - максимальный размер сделки в валюте

* `terms` - публичные условия сделки

* `owner` - публичное автора предложения

* `deepLinkCode` - код для операции deeplink, совпадает с code в параметрах запроса

## GET /prefix/:userId/exchange/dsa/:advertId

Получение публичного описания предложения с учётом идентификатора пользователя `userId` Формат ответа совпадает с
форматом ответа маршрута `GET /prefix/public/exchange/dsa/:advertId`, за исключением дополнительных полей:

* `sellingMax` - реальный верхний лимит по объявлению, учитывающий как верхний лимит по объявлению, так и баланс средств
  на кошельке пользователя `userId`. Если у пользователя на кошельке меньше средств, чем нижний лимит по объявлению, то
  `sellingMax` должен быть равен `0`. Т.е. это сколько `userId` фактически может продать по данному объявлению. Поле
  устанавливается только для объявлений на покупку.
  
* `cryptocurrency.sellingMax` - реальный верхний лимит по объявлению в криптовалюте

# Сделки

## GET /prefix/:userId/trade/?cryptocurrency=:cryptocurrency

Получение описания активных сделок пользователя. В результат попадают
все сделки со статусом, отличным от `archived`.

* `cryptocurrency` - если указан, то необходимо вернуть только сделки купли/продажи указанной криптовалюты.

Формат ответа:

    [
      {
        id: ...,
        partner: ...,
        type: ...,
        currency: {
          code: ...,
          amount: ...
        },
        cryptocurrency: {
          code: ...,
          amount: ...
        },
        rate: ...,
        paymethod: ...,
        status: ...
      },
      ...
    ]
      
    
* `id` - идентификатор сделки

* `partner` - публичное имя партнёра по сделке

* `type` - тип сделки

* `currency.code` - код используемой валюты

* `currency.amount` - размер сделки в используемой валюте

* `cryptocurrency.code` - код используемой криптовалюты

* `cryptocurrency.amount` - размер сделки в используемой
  криптовалюте
  
* `rate` - курс, испульзуемый в сделке

* `paymethod` - текстовое описание метода оплаты

* `total` - общее колличество записей

## POST /prefix/:userId/trade/

Создание новой сделки. Формат запроса:

* `advertId` - идентификатор объявления

* `amount` - размер сделки в валюте, указанной в объявлении

* `counterDetails` - условия и реквизиты принимающего предолжение

* `rate` - значения курса, которое пользователь видел при создании
  объявления. Если с тех пор курс изменился (автором или
  автоматически), то необходимо выдавать ошибку `463`
  
* `amountType` - тип размера сделки, указывается названием валюты, объем которой предполагается

При создании сделки необходимо контролировать, что бы пользователи
имели не более одной активной сделки (не важно в каких
валютах/криптовалютах), и при попытке создать необходимо выдавать
ошибку `470`.
  
## POST /prefix/:userId/trade/idle

Производит "оценку" результатов сделки. Формат запроса:

* `advertId` - идентификатор объявления

* `amount` - размер сделки в валюте или криптовалюте

* `amountType` - тип размера сделки, указывается названием валюты, объем которой предполагается

* `rate` - значения курса, которое пользователь видел при создании
  объявления. Если с тех пор курс изменился (автором или
  автоматически), то необходимо выдавать ошибку `463`

Формат ответа:

    {
      type: ...,
      currency: {
        code: ...,
        amount: ...,
        paymethod: ...
      },
      cryptocurrency: {
         code: ...,
         amount: ...
      },
      fee: ...
    }

* `type` - тип сделки (для пользователя `userId`), возможные значения: `selling` или `purchase`

* `currency` - описание используемой валюты и способа оплаты

* `currency.code` - код валюты

* `currency.amount` - размет сделки в выбранной валюте

* `currency.paymethod` - текстовое описание метода оплаты (например, `Альфа-Банк`)
    
* `cryptocurrency` - описание используемой криптовалюты

* `cryptocurrency.code` - код криптовалюты

* `cryptocurrency.amount` - размер сделки в выбранной криптовалюте

* `fee` - размер сервисного сбора в используемой криптовалюте

## GET /prefix/:userId/trade/:tradeId

Получение описания сделки. Формат ответа:

    {
      id: ...,
      type: ...,
      status: ...,
      currency: {
        code: ...,
        amount: ...,
        paymethod: ...
      },
      cryptocurrency: {
         code: ...,
         amount: ...
      }
      rate: ...,
      terms: ...,
      details: ...,
      counterDetails: ...,
      fee: ...,
      history: [
        {
          date: ...,
          status: ...
        },
        ...
      ],
      availableActions: [
          '...',
          ....
      ],
      partner: ...,
      times: {
        created: ...,
        autocancel: ...,
        dispute: ...
      }
    }
    
* `id` - идентификатор сделки

* `type` - тип сделки (для пользователя `userId`), возможные значения: `selling` или `purchase`

* `status` - статус сделки, возможны следующие значения:

    * `trade-created` -  создана новая сделка путём отклика на объявление

    * `confrim-trade` - автор предложения потвердил принятие отклика (т.е. обе
      стороны потвердили запуск сделки)

    * `payment` - покупатель осуществил платёж

    * `confirm-payment` - продавец потвердил поступление платежа. Сделка завершена

    * `dispute` - открыт диспут по сделке

    * `cancel` - сделка отменена

* `currency` - описание используемой валюты и способа оплаты

* `currency.code` - код валюты

* `currency.amount` - размет сделки в выбранной валюте

* `currency.paymethod` - текстовое описание метода оплаты (например, `Альфа-Банк`)
    
* `cryptocurrency` - описание используемой криптовалюты

* `cryptocurrency.code` - код криптовалюты

* `cryptocurrency.amount` - размер сделки в выбранной криптовалюте

* `rate` - курс, используемый при расчёте сделки

* `terms` - публичные условия из объявления, на основе которого была
  создана сделка

* `details` - закрытые условия (например, реквизиты) из объявления, на
  основе которого была создана сделка
  
* `counterDetails` - условия, указанные при принятии предложения
  
* `fee` - размер сервисного сбора в используемой криптовалюте
  
* `history` - история изменения статусов сделки

* `history.date` - время события изменение статуса

* `history.status` - статус сделки

* `availableActions` - массив возможных кодов действий по
  сделке. Используется в качестве параметре `type` маршрута `POST /prefix/trade/:userId/:tradeId`.
  
    - `addtime` - второй участник сделки запрашивает дополнительные 10 минут для принятия решения

    - `confrim-trade` - второй участник сделки потверждает её начало,
      после этого происходит блокировка монет на счету продавца

    - `payment` - покупатель указывает, что осуществил платёж

    - `confirm-payment` - продавец указывает, что получил платёж, после
      это происходит перевод монет на счёт покупателю, а статус сделки
      меняется на `completed`

    - `dispute` - открытие спора

    - `cancel` - отмена сделки, может быть выполнена покупателем до
      потверждения осуществления платежа

    - `feedback` - по сделке можно оставить отзыв, после чего она
      перейдёт в статус `archived`
      
    - `describe-dispute` - когда сделка находится в диспуте,
      пользователь может один раз описать причину создания диспута

* `partner` - публичное имя партнёра по сделке

* `times` - информация о временных метриках сделки

* `times.created` - время создания сделки

* `times.autocancel` - время, когда сработает автоотмена сделки

* `times.dispute` - время, когда станет доступен диспут по сделке

## PUT /prefix/:userId/trade/:tradeId

Позволяет изменить параметры сделки.

    {
      details: ...
    }
    
* `details` - реквизиты сделки, позволяет установить реквизиты в
  случае, если сделка требует подтверждения и в объявлении на продажу
  не было реквизитов.
  
## POST /prefix/:userId/trade/:tradeId

Выполнение действия в рамках сделки. Формат запроса:

    {
      type: ...,
    }

*  `type` -  код  действия, см.  параметр `availableActions`  маршрута
  `POST /prefix/trade/:userId/:tradeId`.
  
## POST /prefix/:userId/trade/:tradeId/timeout

Вызывается для того, что бы отложить автоотмену сделки на указанный
период времени. Формат запроса:

    {
      timeout: ...
    }
    
* `timeout` - длительность таймаута в минутах. Сервер должен
  проверять, что переданное значение не превышает некое максимальное
  значение. (10 мин)

## PUT /prefix/:userId/trade/:tradeId/feedback

Позволяет поставить оценку по результатам сделки. Формат запроса:

    {
      rate: ...
    }

* `rate` - код оценки из маршрута `GET /prefix/refs/ratings`


## POST /prefix/:userId/trade/:tradeId/dispute/description

Позволяет описать причину создания диспута. В рамках диспута может
быть вызывано только один раз.

Формат запроса:

    {
      description: ...
    }
     
* `description` - описание причины создания диспута


## POST /admin/trade/:tradeId/dispute/sendMessage

Отправить сообщение от админа к участнику трейда

Payload format: 

```
 {
   message: String,
   to: telegramId
 }
```

* `message` -- тело сообщения

* `to` -- telegramId участника спора 

# Deep Linking

## POST /prefix/public/deeplink/

Тоже самое, что и `POST /prefix/:userId/deeplink/`, но для
неавторизованного пользователя.

## POST /prefix/:userId/deeplink/

Вызов действия по коду. Формат запроса:

    {
      code: ...,
      url: ...
    }
    
* `code` - некий код действия, переданный через
  https://core.telegram.org/bots#deep-linking (или какой-либо другой
  механизм)
  
* `url` - полный `url`, по которому зашёл пользователь. Параметр является опциональным и будет передавться только при
  входе через веб.
  
Формат ответа:

    {
      action: ...,
      params: ...
    }
    
* `action` - тип действия, которое должен совершить бот

* `params` - параметры действия, которые зависят от `action`

Возможные значения `action` описаны ниже

### null

Бот ни делает никаких дополнительных действий

### alert 

Бот должен показать сообщение. Параметры:

    {
      message: ...,
    }
    
* `message` - текст сообщения

### showUser

Показать описание пользователя:

    {
      publicName: ...
    }
    
* `publicName` - публичное имя пользователя


### showTrade

Показать состояние сделки

    {
      tradeId: ...
    }
    
* `tradeId` - идентификатор сделки

### showAdvert

Показать объявление с возможность начать сделку

    {
      advertId: ...
    }

* `advertId` - идентификатор объявления

# Чеки

## GET /prefix/:userId/vouchers/cryptocurrency=:cryptocurrency&limit=:limit&skip=:skip

Получение списка активных чеков пользователя. Параметры запроса:

* `userId` - идентификатор пользователя

* `cryptocurrency` - код криптовалюты

* `limit` - ограничение на количество возвращаемых записей

* `skip` - количество записей, которые необходимо пропустить

Формат ответа:

{
  data: [
    {...},
    ...
  ],
  total: ...
}

* `data` - массив описаний чеков, формат описания которых совпадает с
  форматом ответа маршрута `GET /prefix/checks/:userId/:code`

* `total` - общее колличество активных чеков пользователя `userId`

## POST /prefix/:userId/vouchers/

Создание нового чека. Формат запроса:

    {
      cryptocurrency: ...,
      amount: ...,
      currency: ...,
      method: ...
    }
    
* `cryptocurrency` - код криптовалюты

* `currency` - опциональный код валюты, для которой необходимо произвести оценку

* `amount` - размер чека

* `method` - опциаональный метод, на основе которого должен
  производиться расчёт размера чека, возможные значения: `crypto` и `fiat`

Если переданы как `cryptocurrency`, так и `currency`, а сервер не
может определить, как именно производить расчёт чека, то он долже
выдать ошибку `465 Unobvious Requirements`.

В случае успешной обработки запроса, формат ответа совпадает с
форматом ответа маршрута `GET /prefix/checks/:userId/:code`

## GET /prefix/:userId/vouchers/:code?currency=:currency

Получение описание активного чека по коду (`code`) операции
`deeplink`.

Параметры запроса:

* `userId` - идентификатор автора чека

* `code` - код чека

* `currency` - опционально, код валюты, для которой надо сделать
  оценку чека, если не указан, то использовать валюты из профиля

Формат ответа:

    {
      deepLinkCode: ...,
      currency: {
        code: ...,
        amount: ...,
      },
      cryptocurrency: {
        code: ...,
        amount: ...
      },
      links: [
        {
          type: ...,
          url: ...
        },
        ...
      ],
      createdAt: ...
    }
    
* `deepLinkCode` - код для операции `deeplink`, совпадает с `code` в
  параметрах запроса

* `currency` - описание чека в фиатных деньгах

* `currency.code` - код валюты

* `currency.amount` - размер чека (ориентировочный) в валюте

* `cryptocurrency` - описание криптовалюты, для которой выпущен чек

* `cryptocurrency.code` - код криптовалюты

* `cryptocurrency.amount` - размер чека в криптовалюте

* `links` - массив `deeplink` ссылоки

* `links.type` - тип ссылки, возможны значения: `telegram`, `web`

* `links.url` - ссылка

* `createdAt` - дата создания чека

## DELETE /prefix/:userId/vouchers/:code

Отмена неактивированного чека

# Отчёты

## GET /prefix/:userId/transactions/?crytocurrency=:crytocurrency&skip=:skipt&limit=:limit

Получение списка транзакций, отсортированный по убыванию даты создания (сначала последние).

* `crytocurrency` - криптовалюты, для которой необходимо ограничить транзакции (опционально)

* `limit` - ограничение на количество возвращаемых записей

* `skip` - количество записей, которые необходимо пропустить

Формат ответа:

    {
      data: [
        {
          id: ...,
          type: ...,
          created: ...,
          cryptocurrency: {
            code: ...,
            amount: ...
          },
          comment: ...
        },
        ...
      ],
      total: ...
    }

* `data` - массив описаний транзакций в формате, совпадающем с форматом ответа `GET /prefix/:userId/transactions/:txid`

* `total` - общее колличество транзакций

## GET /prefix/:userId/transactions/:txid

Получение информации о транзакции. Формат ответа:

    {
      id: ...,
      type: ...,
      created: ...,
      cryptocurrency: {
        code: ...,
        amount: ...
      },
      address: ...,
      comment: ...,
      viewUrl: ...
    }
    
* `id` - уникальный идентификатор транзакции

* `type` - тип транзакции, `load` (поступление) или `withdrawal` (вывод)

* `created` - дата создания/обработки транзакции

* `cryptocurrency` - размер транзакции в криптовалюты

* `cryptocurrency.code` - код криптовалюты

* `cryptocurrency.amount` - размер транзакции в криптовалюте

* `comment` - комментарий к транзакции

* `viewUrl` - URL по которому можно проверить статус транзакции в blockchain. Если транзакция ещё не вышла в сеть -
  `null`.

## PUT /prefix/:userId/transactions/:txid

Установка комментария к транзакции ввода или вывода средств. Формат запроса:

    {
      comment: ...
    }
    
* `comment` - комментарий 

Формат ответа совпадает с форматом ответа маршрут `GET /prefix/:userId/transactions/:txid`.

## GET /prefix/:userId/reports/

Получение информации о доступных для пользователя отчётах:

    [
      {
        code: ...,
        description: ...,
        format: ...
      },
      ...
    ]
    
* `code` - код отчёт

* `description` - описание отчёта

* `format` - формат отчёта (например, `csv` или `pdf`)

## GET /prefix/:userId/reports/:code

Получение тела отчёта в виде "raw" массива данных.

# Reference book

## GET /prefix/public/refs/cryptocurrencies

List of available cryptocurrencies. Response format:

    [
      {
        code: ...,
        name: ...,
        minWithdrawal: ...
      },
      ...
    ]

* `code` - code of cryptocurrency (`BTC`/`ELT`/`DOGE`/etc)

* `name` - name if cryptocurrency (`Bitcoin`/`Ethereum`/etc)

* `minWithdrawal` - Min widhtorawal amount.

## GET /prefix/public/refs/currencies

List of available currencies. Response format:

    [
      {
        code: ...,
        name: ...,
        sign: ...
      },
      ...
    ]


* `code` - code of currency (`USD`/`RUB`/etc)

* `name` - name of currency (`USA dollar`/`Russian ruble`/etc)

* `sign` - sign (`₽`/`$`/etc) 

## GET /prefix/public/refs/rate-sources?cryptocurrency=:cryptocurrency=&currency=:currency

Список доступных источников курсов криптовалют. Возможно указать два
опциальных параметра для фильтрации источников курсов:

* `cryptocurrency` - код криптовалюты

* `currency` - код валюты

Формат ответа:

    [
      {
        id: ...,
        description: ...,
        url: ...,
        rate: ...
      },
      ...
    ]

* `id` - идентификатор источника курса

* `description` - текстовое описание источника курса

* `url` - опциональная ссылка на сайт источника

* `rate` - текущий курс криптовалюты в валюте (если указаны параметры `crypto` и `currency`)

## GET /prefix/public/refs/paymethods?currency=:currency (реализовано)

Список возможных вариантов оплаты при совершении сделки.

* `currency` - позволяет ограничить список только системами оплаты для
  указанной валюты

Формат ответа:

    [
      {
        id: ...,
        currency: ...,
        description: ...
      },
      ...
    ]

* `id` - идентификатор способа оплаты

* `currency` - код валюты, используемой в данном методе оплаты

* `description` - текстовое описание способа оплаты

## GET /prefix/public/refs/paymethods/:id

Получение описания варианта оплаты по идентификатору. Формат ответа:

    {
      id: ...,
      currency: ...,
      description: ...
    }

* `id` - идентификатор способа оплаты

* `currency` - код валюты, используемой в данном методе оплаты

* `description` - текстовое описание способа оплаты

## GET /prefix/public/refs/ratings 

Получение списка возможных оценок результатов сделки. Формат ответа:

    [
      {
        code: ...,
        emoji: ...
      },
      ...
    ]

* `code` - код оценки

* `emoji` - `unicode`-представление

На данном этапе предлагается использовать следующие значения:

    [
      {
        code: 'thumb-up',
        emoji: '👍'
      },
      {
        code: 'relieved',
        emoji: '😌'
      },
      {
        code: 'weary',
        emoji: '😩'
      },
      {
        code: 'rage',
        emoji: '😡'
      },
      {
        code: 'hankey',
        emoji: '💩'
      }
    ]
    
# О сервисе

## GET /prefix/public/about/friends (реализовано)

Получение описания друзей. Формат ответа:

    [
      {
        name: ...,
        url: ...
      },
      ...
    ]

* `name` - имя друга

* `url` - ссылка

## GET /prefix/public/about/community (реализовано)

Получение описания каналов обсуждений. Формат ответа:

    [
      {
        name: ...,
        url: ...
      },
      ...
    ]

* `name` - имя друга

* `url` - ссылка

# Статистика

## GET /prefix/public/stats/volumes

Получение информации об обороте за последние 24 часа по всем криптовалютам. Формат ответа:

    [
      {
        cryptocurrency: ...,
        volume: ...
      },
      ...
    ]

* `cryptocurrency` - код криптовалюты

* `volume` - оборот за последние 24 часа

## GET /prefix/public/stats/lastdeal

Получение информации о последней сделке. Формат ответа:

     {
       cryptocurrency: ...,
       currency: ...,
       amount: ...,
       rate: ...,
       date: ...
     }

* `cryptocurrency` - код криптовалюты

* `currency` - код валюты

* `amount` - размер сделки в криптовалюте

* `rate` - курс, по которому была совершена сделка

* `date` - дата последней сделки


# Оповещения

Для оповещения заинтересованных клиентов (приложений) о событиях
используется протокол `AMQP` (в реализации `RabbitMQ`), `exchange` с
типом `fanout`. 

* `com.bitzlato.multi` - в него отправляются все оповещения

* `com.bitzlato.doge` - в него отправляются все оповещения, касающиеся
  `DOGE` и те, для которых невозможно определять используемые
  криптовалюту

* `com.bitzlato.eth` - в него отправляются все оповещения, касающиеся
  `Ethereum` и те, для которых невозможно определять используемые
  криптовалюту
  
Для получения непрочитанных оповещений, а так же для пометки как
"прочитанные", используется протокол `HTTP`.

Каждое оповещение должно быть закодированно в формате `JSON`:

    {
      id: ...,
      name: ...,
      data: ...
    }

См. `GET /prefix/:userId/notifications/:id`.


**ВАЖНО**: телеграм-бот будет каждое оповещение помечать как
прочитанное сразу же после получение, но, несмотря на этого, надо
обеспечить, что бы веб-клиент так же получил оповещение в любом случае
(т.е. необходимо избежать ситуации, когда один из приёмников
оповещений помечает его как прочитанное, что препятствует его отправке
в другие источники).


## GET /prefix/:userId/notifications/tokens/

Получение списка токенов пользователя для отправки `push`-уведомлений 

    [
      {
        deviceId: ...,
        token: ...
      },
      ...
    ]
    
Описание элемента массива совпадает с описанием ответа маршрута
`GET /prefix/:userId/notifications/tokens/:deviceId`.

## POST /prefix/:userId/notifications/tokens/:deviceId

Указание для пользователя нового токена. Формат запроса:

    {
      deviceId: ...,
      token: ...
    }

* `deviceId` - уникальный идентификатор устройства (создаётся на
  стороне клиента)
  
* `token` - токен `firebase` для отправки `push`-уведомлений


Формат ответа совпадает с форматом ответа маршрута `GET /prefix/:userId/notifications/tokens/:deviceId`.

## GET /prefix/:userId/notifications/tokens/:deviceId

Получение описание токена для устройства `deviceId`.

    {
      deviceId: ...,
      token: ...
    }

* `deviceId` - уникальный идентификатор устройства 
  
* `token` - токен `firebase` для отправки `push`-уведомлений

## DELETE /prefix/:userId/notifications/tokens/:deviceId

Удаление токена.

## GET /prefix/:userId/notifications/

Получение списка непрочитанных оповещений. Формат ответа:

    [
      {
        id: ...,
        name: ...,
        data: ...
      },
      ...
    ]
    
Формат описания каждого элемента соответствует формату ответа маршрута
`GET /prefix/:userId/notifications/:id`.

## POST /api/bot/:userId/notifications/

Регистрация нового оповещения (доступна только для бота). Формат запроса:

    {
      name: ...,
      data: ...
    }

* `name` - тип оповещения

* `data` - данные оповещения в формате, специфическом для каждого типа
  оповещения
  
Формат ответа совпадает с форматом ответа `GET /prefix/:userId/notifications/:id`.

Данный метод должен только регистрировать оповещение, но не отправлять его в RabbitMQ.


## GET /prefix/:userId/notifications/:id

Получение описания оповещения по его идентификатору.

    {
      id: ...,
      name: ...,
      data: ...
    }
    
* `id` - идентификатор оповещения

* `name` - тип оповещения

* `data` - данные оповещения в формате, специфическом для каждого типа
  оповещения


## DELETE /prefix/:userId/notifications/:id

Оповещение помечается как "прочитанное" и больше не выдаётся в списке
непрочитанных оповещений.

## tradeStatusChanged

Порождается при изменении статуса сделки. Если статус сделки изменён в
результате решения спора, то должно отправляться сообщение
`disputeResolved`

    {
      tradeId: ...,
      status: ...,
      participants: [
         ...
      ]
    }
    
* `tradeId` - идентификатор сделки

* `status` - новый статус

* `participants` - массив идентификаторов участников сделки

## disputeAvailable

Оповещение о том, что по сделке появилась возможность отрыть диспут.

    {
      tradeId: ...,
      participants: [
         ...
      ]
    }
    
* `tradeId` - идентификатор сделки

* `participants` - массив идентификаторов участников сделки

## disputeResolved

Оповещение о результатах спора:

    {
      userId: ...,
      tradeId: ...,
      status: ...,
      cryptocurrency: {
        code: ...,
        amount: ...
      }
    }

* `tradeId` - идентификатор сделки

* `status` - резльутат спора, возможные значения: `win` или `fail`

* `cryptocurrency` - описание размера сделки в критовалюте

* `cryptocurrency.code` - код криптовалюты

* `cryptocurrency.amount` - размер сделки в криптовалюте

## newChatMessage

В чат (сделки) добавлено новое сообщение.

    {
      tradeId: ...,
      from: ...
      isAdmin: ...,
      to: [
      ...
      ],
      message: ...,
    }

* `tradeId` - опциональный идентификатор сделки в случае, если
  сообщение отправлено в рамках сделки, иначе (простое личное
  сообщение) не указывается

* `from` - публичное имя автора сообщения

* `isAdmin` - булев признак того, что автором сообщения является
  администратор

* `to` - массив идентификаторов участников сделки

* `message` - описание сообщения в формате, совпадающем с форматом
  ответа `GET /prefix/:userId/userinfo/:publicName/chat/:id`

## tradeExtendWaitingTime

Продлено время ожидания принятия сделки. 

    {
      userId: ...,
      time ...,
      tradeId ...
    }

* `userId` - идентификтор пользователя

* `time` - время на которое продлено ожидание принятия сделки. (в мс)

* `tradeId` - идентификатор незавершённой сделки

## tradeWillExpire

Уведомление о приближающейся автоматической отмене сделки:

    {
      userId: ...,
      tradeId: ...
      time: ...
    }

* `userId` - идентификтор пользователя

* `tradeId` - идентификатор сделки

* `time` - время, когда должна произойти автоотмена.

## userMessage

Отправка произвольного сообщения пользователю.

    {
      userId: ...,
      message: ...,
      format: ...
    }
    
* `userId` - идентификтор пользователя

* `message` - сообщение

* `format` - формат сообщения, `HTML` или `Markdown`

## userTradeStatusChanged

Оповещение об изменении статуса торговли пользователя.

    {
      userId: ...
      tradeStatus: {
         BTC: ...,
         ETH: ...,
         ....
      },
      reason: ...
    }
    
* `userId` - идентификтор пользователя
    
* `tradeStatus` - текущий статус торговли пользователя по каждой криптовалюте

* `reason` - причина изменения статуса торговли, возможные значения:

    - `manual` - пользователь сам изменил свой статус торговли

    - `admin` - статус торговли пользоватея изменил админстратор

    - `autocancel` - статус изменился с результате автоматической отмены
      сделки

## checkCashed

Оповещение о том, что пользователь "обналичил" чек

    {
      userId: ...,
      recipient: ...,
      cryptocurrency: ...,
      amount: ...
    }

* `userId` - идентификатор создателя чека

* `recipient` - публичное имя пользователя, который воспользовался чеком

* `cryptocurrency` - код криптовалюты

* `amount` - размер чека

## moneyReceived

Оповещение о том, что пользователь получил средства от "кого-то"

    {
      userId: ...,
      donor: ...,
      cryptocurrency: {
        code: ...
        amount: ...
      }
      currency: {
        code: ...,
        amount: ...
      }
    }

* `userId` - идентификатор получателя средств

* `donor` - публичное имя пользователя, от кого получены средства

* `cryptocurrency` - описание полученных средств в криптовалюте

* `cryptocurrency.amount` - размер полученных средств в криптовалюте

* `cryptocurrency.code` - код криптовалюты

* `currency` - описание полученных средств в валюте

* `currency.amount` - размер полученных средств в валюте

* `currency.code` - код валюты

## newReferral

Оповещение о том, что у пользователя появился новый реферал:

    {
      userId: ...,
      referral: ...
    }

## dividendsReceived

Оповещение о получении вознаграждения за сделки рефералов:

    {
      userId: ...,
      cryptocurrency: {
        code: ...,
        amount: ...
      }
    }
    
## accountsMerged

Оповещение о меже аккаунтов веб и телеграм:

    {
      userId: ...
    }

* `userId` - идентификатор пользователя

## blockChainMoneyReceived

Оповещение о получении получении средств из внешней сети:

    {
      userId: ...,
      cryptocurrency: ...,
      amount: ...,
      txid: ...
    }

* `userId` - идентификатор пользователя

* `cryptocurrency` - код криптовалюты

* `amount` - количество поступивших средств

* `txid` - идентификатор транзакции

## blockChainMoneySent

Оповещение об отправке средств на внешний адрес:

    {
      userId: ...,
      cryptocurrency: ...,
      amount: ...,
      txid: ...,
      address: ...
    }

* `userId` - идентификатор пользователя

* `cryptocurrency` - код криптовалюты

* `amount` - количество фактически отправленных средств

* `txid` - идентификатор транзакции

* `address` - внешний адрес, на который произведена отправка средств

## blockChainTransactionConfirmed

Оповещение о том, что платёж получил потверждение сети:

    {
      userId: ...
      cryptocurrency: ...,
      amount: ...,
      txid: ...,
      address: ...,
      confirmCount: ...
    }

* `userId` - идентификатор пользователя

* `cryptocurrency` - код криптовалюты

* `amount` - количество фактически отправленных средств

* `txid` - идентификатор транзакции

* `address` - внешний адрес, на который произведена отправка средств

* `confirmCount` - количество подтверждений сети

## newDeal

Получение информации о новой сделке. Формат данных совпадает с форматом ответа маршрута
`GET /prefix/public/stats/lastdeal`:

    {
      cryptocurrency: ...,
      currency: ...,
      amount: ...,
      rate: ...,
      date: ...
    }

* `cryptocurrency` - код криптовалюты

* `currency` - код валюты

* `amount` - размер сделки в криптовалюте

* `rate` - курс, по которому была совершена сделка

* `date` - дата последней сделки


## verificationDecision

Оповещение о решении по результатам рассмотрения заявки на верификацию

    {
      userId: ...,
      status: ...,
      reason: ...
    }

* `userId` - идентификатор пользователя

* `status` - булев признак того, что верификация была подтверждена

* `reason` - причина отказа (если `status` равен `false`)

## mute

Оповещение о том, что на пользователя наложен mute

    {
      userId: ..
      duration: ...,
      reason: ...,
      expiry: ...
    }
    
* `userId` - идентификатор пользователя

* `duration` - длительность ограничения (`1h`, `4h`, `12h`, `24h`)

* `reason` - текстовое описание причины, по которой на пользователя был наложен mute

* `expiry` - дата окончания действия ограничения
