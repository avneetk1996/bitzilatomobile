export default class Singleton {

    static myInstance = null;

    userId = "";


    /**
     * @returns {CommonDataManager}
     */
    static getInstance() {
        if (Singleton.myInstance == null) {
            Singleton.myInstance = new Singleton();
        }
        return this.myInstance;
    }

    getUserId() {
        return this.userId;
    }

    setUserId(id) {
        this.userId = id;
    }
}
