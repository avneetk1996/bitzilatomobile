// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round

/**
 * Decimal adjustment of a number.
 *
 * @param   {String}    type    The type of adjustment.
 * @param   {Number}    value   The number.
 * @param   {Integer}   exp     The exponent (the 10 logarithm of the adjustment base).
 * @returns {Number}            The adjusted value.
 */
export function decimalAdjust(type, value, exp = -8) {
    // If the exp is undefined or zero...
    if (typeof exp === 'undefined' || +exp === 0) {
        return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
        return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? +value[1] - exp : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? +value[1] + exp : exp));
}

export const round10 = (value, exp) => decimalAdjust('round', value, exp);
export const floor10 = (value, exp) => decimalAdjust('floor', value, exp);
export const ceil10 = (value, exp) => decimalAdjust('ceil', value, exp);

// https://stackoverflow.com/questions/175739/built-in-way-in-javascript-to-check-if-a-string-is-a-valid-number
export const isNumber = str => {
    if (typeof str != 'string') return false; // we only process strings!
    // could also coerce to string: str = ""+str
    return !isNaN(str) && !isNaN(parseFloat(str));
};

export const isNumberComma = str => isNumber(typeof str === 'string' ? str.replace(',', '.') : str);

export const strToNumComma = str => Number(typeof str === 'string' ? str.replace(',', '.') : str);

export const formatNumber = number =>
    Number(number)
        .toFixed(9)
        .replace(/\.?0+$/, '');
