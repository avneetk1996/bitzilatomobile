import React from 'react';
import { View, TouchableOpacity, Image, Button, Modal, Text, StyleSheet, Platform } from 'react-native';
import { withNavigation } from 'react-navigation';
import MarqueeText from 'react-native-marquee';
import Singleton from '../utils/Singleton';
import { Icon } from 'native-base';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = { currency1: '', currency2: '', ModalVisibleStatus: false };
    }

    handleModalVisibility(visible) {
        this.setState({ ModalVisibleStatus: visible });
    }

    componentDidMount() {
      //  const mThis = this;
        fetch('https://bitzlato.com/api/p2p/public/stats/lastdeal')
            .then(res => {
                var obj = JSON.parse(res._bodyInit);
                this.setState({ currency1: obj.amount, currency2: obj.rate });
            })
            .catch(error => {
                console.log('lastError:', error);
            });

        setInterval(function() {
            fetch('https://bitzlato.com/api/p2p/public/stats/lastdeal')
                .then(res => {
                    var obj = JSON.parse(res._bodyInit);
                    this.setState({ currency1: obj.amount, currency2: obj.rate });
                })
                .catch(error => {
                    console.log('lastError:', error);
                });
        }, 60000);
    }

    onPressLogin() {
        this.props.navigation.navigate('Login');
    }

    renderButton() {
        let singleton = Singleton.getInstance();
        let userId = singleton.getUserId();
         if(String(userId).length !==0)
        return (
            <TouchableOpacity
                style={{ backgroundColor: '#fff', borderRadius: 70, padding: 4 }}
                onPress={this.onUserPress.bind(this)}
            >
                <Icon name="person" style={{ fontSize: 20 }} />
            </TouchableOpacity>
        );
           return <Button
           onPress={this.onPressLogin.bind(this)}
            title="LOGIN"
            color="#CCCC00"
         />;
    }

    onCrossPressed() {
        this.handleModalVisibility(false);
    }

    handleNavigation(route) {
        this.handleModalVisibility(false);
        this.props.navigation.navigate(route);
    }

    onUserPress() {
        this.handleModalVisibility(true);
    }

    render() {
        return (
            <View style={styles.HeaderContainer}>
                <Modal
                    transparent={true}
                    animationType={'fade'}
                    visible={this.state.ModalVisibleStatus}
                    onRequestClose={() => {
                        this.handleModalVisibility(!this.state.ModalVisibleStatus);
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            marginTop: 50,
                            justifyContent: 'flex-end',
                            alignItems: 'flex-start',
                            flex: 1
                        }}
                    >
                        <View style={styles.ModalInsideView}>
                            {/* Put All Your Components Here, Which You Want To Show Inside The Modal. */}
                            <Text onPress={() => this.handleNavigation('Home')} style={styles.TextStyle}>
                                Personal Area
                            </Text>
                            <Text style={styles.TextStyle}>Deals</Text>
                            <Text style={styles.TextStyle}>Wallets</Text>
                            <Text onPress={() => this.handleNavigation('Announcements')} style={styles.TextStyle}>
                                My announcements
                            </Text>
                            <Text style={styles.TextStyle}>Instruments</Text>
                            <TouchableOpacity onPress={() => this.handleNavigation('Login')}>
                                <Text style={styles.TextStyle}>Logout</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.onCrossPressed.bind(this)}>
                                <Image
                                    style={{ height: 20, width: 20 }}
                                    source={require('../assets/images/cross.png')}
                                />
                            </TouchableOpacity>
                            {/* Put All Your Components Here, Which You Want To Show Inside The Modal. */}
                        </View>
                    </View>
                </Modal>

                <View style={styles.HeaderInnerContainer}>
                    <Image
                        resizeMode="contain"
                        style={{ height: 40, width: 160, marginTop: 8 }}
                        source={require('../assets/images/bitzlato.png')}
                    />

                    <View style={{ alignItems: 'flex-end', flex: 1, margin: 4 }}>{this.renderButton()}</View>
                </View>

                <MarqueeText
                    style={{ fontSize: 16, color: 'white', paddingLeft: 8, paddingRight: 8 }}
                    duration={4000}
                    marqueeOnStart
                    loop
                >
                    {this.state.currency1} BTC transaction was completed at the rate of {this.state.currency2} RUB a
                    minute ag
                </MarqueeText>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    HeaderContainer: {
        backgroundColor: '#233070',
        height: Platform.OS == 'ios' ? 100 : 70
    },

    HeaderInnerContainer: {
        flexDirection: 'row',
        flex: 1,
        marginTop: Platform.OS == 'ios' ? 30 : 0
    },

    MainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: Platform.OS == 'ios' ? 20 : 0
    },

    ModalInsideView: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        height: 260,
        width: '60%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#000'
    },

    TextStyle: {
        fontSize: 18,
        marginBottom: 2,
        color: 'black',
        padding: 5,
        textAlign: 'center'
    }
});

export default withNavigation(Header);
