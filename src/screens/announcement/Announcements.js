import React from 'react';
import { View, Text, FlatList, TouchableOpacity, AsyncStorage } from 'react-native';
import { H2, Button, Card, CardItem } from 'native-base';
import {connect} from "react-redux";
import {bindActionCreators}from "redux";
import {loadMainRefs}from "../../redux/actions/refs";
import {store}from "../../../App"
import {loadAdverts, loadOneAdvert} from '../../redux/actions/adverts';
import { ScrollView } from 'react-native-gesture-handler';


var purchaseAds=[], sellingAds=[];

class Announcements extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listData: ['DOGE', 'DASH', 'LTC', 'BCH', 'ETH', 'BTC'],
            apiDone:true
        };
    }

    handleNavigation(route) {
        this.props.navigation.navigate(route);
        store.dispatch(loadMainRefs());
    }

    componentDidMount(){
        AsyncStorage.getItem('TOKEN', (err, result) => {
                if(result !== null){
                    AsyncStorage.getItem('USERID', (err, result1) => {
                        if(result1 !== null){
                            console.log('failed:', result1+'     '+result)
                            store.dispatch(loadAdverts(result1, result));
                        }
                    });
                }
            });
    }

    componentWillReceiveProps(){
        if(this.props.navigation.state.params === undefined){
            
        }else{
            if(this.state.apiDone){
                this.setState({apiDone:false})
                AsyncStorage.getItem('TOKEN', (err, result) => {
                if(result !== null){
                    AsyncStorage.getItem('USERID', (err, result1) => {
                        if(result1 !== null){
                            console.log('failed:', result1+'     '+result)
                            store.dispatch(loadAdverts(result1, result));
                        }
                    });
                }
            });
        }
    }
    }

    advertsData(data){
        var purchaseAdverts=[]
        var sellingAdverts=[]
        for(let i=0; i<data.length; i++){
           if(data[i].type === 'purchase'){
               purchaseAdverts.push(data[i])
           }else{
                sellingAdverts.push(data[i])
           }
        }        
        purchaseAds=purchaseAdverts;
        sellingAds=sellingAdverts;
    }

    render() {
        this.advertsData(this.props.advert.all)

        const { listData } = this.state;
        return (
            <View>
                <View style={styles.heading}>
                    <H2>Here is a list of your ads</H2>
                </View>
                <View>
                    <FlatList
                        horizontal={true}
                        data={listData}
                        keyExtractor={(item, index) => index}
                        renderItem={({ item }) => (
                            <TouchableOpacity style={styles.listItem}>
                                <Text style={{ color: '#000' }}>{item}</Text>
                            </TouchableOpacity>
                        )}
                    />
                </View>
                <Button onPress={()=>this.handleNavigation("AddAnnouncement")} style={styles.createButton} full>
                    <Text style={styles.buttonText}>ADD ANNOUNCEMENT</Text>
                </Button>
                <ScrollView>
                    <View style={{flexDirection:'column'}}>
                        <View style={{ padding: 10 }}>
                            <Card>
                                <CardItem header>
                                    <View style={{ alignItems: 'center', flex: 1 }}>
                                        <Text>PURCHASE</Text>
                                    </View>
                                </CardItem>
                                <CardItem bordered>
                                    <View style={{flexDirection:'column', flex:1}}>
                                        <View style={{ flexDirection: 'row'}}>
                                            <Text style={{ flex: 0.5 }}>Status</Text>
                                            <Text style={{ flex: 0.5 }}>Currency</Text>
                                            <Text style={{ flex: 0.5 }}>Course</Text>
                                        </View>
                                        <View style={styles.divider}/>
                                        <FlatList
                                            style={{flex:1}}
                                            data={purchaseAds}
                                            keyExtractor={(item, index) => index}
                                            renderItem={({ item }) => (
                                                    <View style={{flexDirection:'column'}}>
                                                        <View style={{ flexDirection: 'row', marginTop:12}}>
                                                            <Text style={{ flex: 0.5 }}>{item.status}</Text>
                                                            <Text style={{ flex: 0.5 }}>{item.paymethod.name+' ( '+item.paymethod.currency+' )'}</Text>
                                                            <Text style={{ flex: 0.5 }}>{item.rate.value+' '+item.paymethod.currency+'/'+item.cryptocurrency}</Text>
                                                        </View>
                                                        <View style={styles.divider}/>
                                                    </View>
                                            )}
                                        />
                                    </View>
                                </CardItem>
                            </Card>
                        </View>
                        <View style={{ padding: 10 }}>
                            <Card>
                                <CardItem header>
                                    <View style={{ alignItems: 'center', flex: 1 }}>
                                        <Text>SALE</Text>
                                    </View>
                                </CardItem>
                                <CardItem bordered>
                                    <View  style={{flexDirection:'column', flex:1}}>
                                        <View style={{ flexDirection: 'row'}}>
                                            <Text style={{ flex: 0.5 }}>Status</Text>
                                            <Text style={{ flex: 0.5 }}>Currency</Text>
                                            <Text style={{ flex: 0.5 }}>Course</Text>
                                        </View>
                                        <View style={styles.divider}/>
                                            <FlatList style={{flex:1, height:'100%'}}
                                                data={sellingAds}
                                                keyExtractor={(item, index) => index}
                                                renderItem={({ item }) => (
                                                        <View style={{flexDirection:'column'}}>
                                                            <View style={{ flexDirection: 'row', marginTop:12}}>
                                                                <Text style={{ flex: 0.5 }}>{item.status}</Text>
                                                                <Text style={{ flex: 0.5 }}>{item.paymethod.name+' ( '+item.paymethod.currency+' )'}</Text>
                                                                <Text style={{ flex: 0.5 }}>{item.rate.value+' '+item.paymethod.currency+'/'+item.cryptocurrency}</Text>
                                                            </View>
                                                            <View style={styles.divider}/>
                                                        </View>
                                                )}
                                            />
                                    </View>
                                </CardItem>
                            </Card>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = {
    headerContainer: {
        flexDirection: 'row',
        marginTop: 8
    },
    heading: {
        padding: 10
    },
    divider:{
        backgroundColor:'#000000', 
        height:0.2 ,
         flex:1,
          flexDirection:'row',
         marginTop:8
    },
    listItem: {
        padding: 17,
        borderColor: 'gray',
        borderBottomWidth: 2
    },
    createButton: {
        backgroundColor: '#233070',
        borderRadius: 6,
        height: 40,
        margin: 6
    },
    buttonText: {
        color: '#fff',
        fontSize: 16,
        textDecorationLine: 'underline'
    }
};



const mapStateToProps=(state)=>{
    return {
        advert:state.adverts,
        state
    }
}

const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({loadMainRefs, loadAdverts, loadOneAdvert},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Announcements);
