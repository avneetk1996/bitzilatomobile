import React from 'react';
import { View, Text, FlatList, ScrollView, Platform, AsyncStorage } from 'react-native';
import { ListItem, List, H3, Picker, Item, Label, Input, Icon, Form, Button } from 'native-base';
import {connect} from "react-redux";
import {createAdvert} from  "../../redux/actions/adverts";
import {bindActionCreators}from "redux";
import IOSPicker from 'react-native-ios-picker';
import {loadDsaPaymethodsRef} from '../../redux/actions/refs';
import {store}from "../../../App"
import {StackActions, NavigationActions} from 'react-navigation'

var cryptocurrencies=[];
var currencies=[];
var paymethods=[];
var currency='', crypto='', adType='', paymethod='';

class AddAnnouncement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tableData: [{ first: 'data1', second: 'data2', third: 'data3' }],
            adTypeData: [{ code: 'selling', name: 'To Sell' }, { code: 'purchase', name: 'Buy' }],
            adTypeAndroidData:[{ code: 'Select', name: 'Select' }, { code: 'selling', name: 'To Sell' }, { code: 'purchase', name: 'Buy' }],
            cryptocurrencies: [],
            currencies: [],
            currency:'select',
            crypto:'select',
            adType:'select',
            paymethod:'select',
            currencyValue:'',
            cryptoValue:'',
            adTypeValue:'',
            paymethodValue:'',
            percent:'',
            minAmount:'',
            maxAmount:'',
            terms:'',
            requisite:'',
            manualPercent:'',
            moved:false,
            currenyAdded:true,
            cryptoAdded:true
        };
    }

    handleNavigation(route) {
        // const resetAction = StackActions.reset({
        //     index: 0,
        //     actions: [NavigationActions.navigate({ routeName: route })],
        //     });
        //     this.props.navigation.dispatch(resetAction)
        this.props.navigation.navigate(route, {
            from: 'addAnnouncement'
          });
    }

    renderPickerItem(data){
        return data.map((item, key) => {
                return ( 
                            <Picker.Item label={item.name} key={key} value={item.name}  />
                        );
            })
        
    }

    renderPaymentPickerItem(data){
        return data.map((item, key) => {
            return ( 
                        <Picker.Item label={item.description} key={key} value={item.description}  />
                    );
        })        
   
    }

    dropDownRenderer(data, type){
        if (type === 'payment'){
            if(Platform.OS === 'ios'){
                return  <View  style={styles.pickerStyle}>
                <IOSPicker 
                    mode='modal'
                    style={{ height: 40 , width:180}}
                    selectedValue={this.selectedValue(type)}
                    onValueChange={(itemValue, itemIndex) =>
                        this.dropDownSelected(itemValue, type, itemIndex)
                    }>
                        {this.renderPaymentPickerItem(data)}
                    </IOSPicker> 
                    </View>;
            }else{
                return <View  style={{flex:1}}>
                <Picker style={{ height: 40 , width:'100%'}}
                selectedValue={this.selectedValue(type)}
                onValueChange={(itemValue, itemIndex) =>
                    this.dropDownSelected(itemValue, type,itemIndex)
                }>
                {this.renderPaymentPickerItem(data)}
                </Picker> 
                </View> ;
           }

        }
        else if(type === 'adType'){
                if(Platform.OS === 'ios'){
                    return  <View  style={styles.pickerStyle}>
                    <IOSPicker 
                        mode='modal'
                        style={{ height: 40 , width:180}}
                        selectedValue={this.selectedValue(type)}
                        onValueChange={(itemValue, itemIndex) =>
                            this.dropDownSelected(itemValue, type, itemIndex)
                        }>
                            {this.renderPickerItem(data)}
                        </IOSPicker> 
                        </View>;
                }else{
                    data= this.state.adTypeAndroidData;
                
                    return <View  style={{flex:1}}>
                    <Picker style={{ height: 40, width:'100%'}}
                    selectedValue={this.selectedValue(type)}
                    onValueChange={(itemValue, itemIndex) =>
                        this.dropDownSelected(itemValue, type, itemIndex)
                    }>
                    {this.renderPickerItem(data)}
                    </Picker> 
                    </View> ;
               }
            
        }
        else{
            if(Platform.OS === 'ios'){
                return  <View  style={styles.pickerStyle}>
                <IOSPicker 
                    mode='modal'
                    style={{ height: 40 , width:180}}
                    selectedValue={this.selectedValue(type)}
                    onValueChange={(itemValue, itemIndex) =>
                        this.dropDownSelected(itemValue, type, itemIndex)
                    }>
                        {this.renderPickerItem(data)}
                    </IOSPicker> 
                    </View>;
            }else{
                return <View  style={{flex:1}}>
                <Picker style={{ height: 40, width:'100%'}}
                selectedValue={this.selectedValue(type)}
                onValueChange={(itemValue, itemIndex) =>
                    this.dropDownSelected(itemValue, type, itemIndex)
                }>
                {this.renderPickerItem(data)}
                </Picker> 
                </View> ;
           }
        }

    };

    selectedValue(type){
       if(type === 'crypto'){
           return this.state.crypto
        }else if(type === 'currencies'){
           return this.state.currency
        }else if(type === 'adType'){
            return this.state.adType
        }else if(type === 'payment'){
            return this.state.paymethod
        }
    }

    dropDownSelected(itemValue, type, itemIndex){
        if(type === 'currencies'){
            const value = currencies[itemIndex].code
            currency= value
            this.setState({ currency: itemValue , currencyValue : value})
        }else if(type === 'crypto'){
            const value = cryptocurrencies[itemIndex].code
            crypto=value
            this.setState({ crypto: itemValue, cryptoValue:value })
        }else if(type === 'adType'){
            if(Platform.OS === 'android'){
                const value = this.state.adTypeAndroidData[itemIndex].code
                adType=value
                this.setState({adType:itemValue, adTypeValue:value})
            }else{
                const value = this.state.adTypeData[itemIndex].code
                adType=value
                this.setState({adType:itemValue, adTypeValue:value})
            }
           
        }else if(type === 'payment'){
            const value = paymethods[itemIndex].id
            paymethod=value
            this.setState({paymethod:itemValue, paymethodValue:value})
        }
        this.callPaymentMethodsApi()
    }

    callPaymentMethodsApi(){
        if(crypto === '' || crypto === 'Select'){
            console.log('crypto missing')
        }else if(currency === '' || crypto === 'Select'){
            console.log('currency missing')
        }else if(adType === '' || crypto === 'Select'){
            console.log('adtype missing')
        }else{
            AsyncStorage.getItem('TOKEN', (err, result) => {
                if(result !== null){
                    AsyncStorage.getItem('USERID', (err, result1) => {
                        if(result1 !== null){
                            store.dispatch(loadDsaPaymethodsRef(adType, currency, crypto, result1, result));
                        }
                    });
                }
            });
            
        }

    }

    receiveProps(){
        
      console.log('references:', this.props)
        if (!Object.keys(this.props.refs).length == 0) {
            if(this.props.refs.cryptocurrencies !== undefined){
                var object={'code':'Select', 'name':'Select'}            
                cryptocurrencies=[]
                cryptocurrencies = this.props.refs.cryptocurrencies; 
                if(Platform.OS === 'android'){
                     if(this.state.cryptoAdded){
                        this.setState({cryptoAdded:false})
                        cryptocurrencies.splice(0,0,object)
                    }
                }
            }
            if(this.props.refs.currencies !== undefined){
                var object={'code':'Select', 'name':'Select'}     
                currencies=[]
                currencies= this.props.refs.currencies; 
                if(Platform.OS === 'android'){
                    if(this.state.currenyAdded){
                        this.setState({currenyAdded:false})
                        currencies.splice(0,0,object)
                }
                }
            }
            if(this.props.refs.dsapaymethods !== undefined){
                paymethods=[]
                paymethods= this.props.refs.dsapaymethods.dsa_paymethods;
                console.log('payments:::', paymethods)
            }
        }
        if(!Object.keys(this.props.advert).length == 0){
            if(!Object.keys(this.props.advert.items).length == 0){
                if(this.state.moved){
                    this.setState({moved:false})
                    this.handleNavigation('Announcements')
                }
            }
        }
    }

    onChangePercentage(text){
        this.setState({percent:text})
    }

    onChangeManualPercentage(text){
        this.setState({manualPercent:text})
    }

    onChangeMinAmount(text){
        this.setState({minAmount:text})
    }

    onChangeMaxAmount(text){
        this.setState({maxAmount:text})
    }

    onChangeTerms(text){
        this.setState({terms:text})
    }

    onChangeRequisite(text){
        this.setState({requisite:text})
    }

    hitCreateAdvertApi(){
        const {cryptoValue, adTypeValue, minAmount, maxAmount, terms, requisite, percent, paymethodValue, manualPercent} = this.state;
        console.log('this.state:', this.state)
        AsyncStorage.getItem('TOKEN', (err, result) => {
            if(result !== null){
                AsyncStorage.getItem('USERID', (err, result1) => {
                    if(result1 !== null){
                        store.dispatch(createAdvert({type:adTypeValue, cryptocurrency:cryptoValue, paymethod:paymethodValue, rateValue:percent, ratePercent:manualPercent, minAmount, maxAmount, terms, details:requisite, userId:result1, token:result}));
                    }
                });
            }
        });
        this.setState({moved:true})
    }

    render() {

        this.receiveProps()

        const {tableData, adTypeData} = this.state;
        return (
            <ScrollView>
                <List>
                    <ListItem style={{ borderBottomWidth: 1 }} itemDivider>
                        <View style={styles.headerContainer}>
                            <Text style={styles.listHeader}>Balance</Text>
                            <Text style={styles.listHeader}>About</Text>
                            <Text style={styles.listHeader}>Blocked</Text>
                        </View>
                    </ListItem>
                    <FlatList
                        data={tableData}
                        keyExtractor={(item, index) => index}
                        renderItem={({ item }) => (
                            <ListItem style={{ borderBottomWidth: 1 }} itemDivider>
                                <View style={styles.headerContainer}>
                                    <Text style={styles.listHeader}>{item.first}</Text>
                                    <Text style={styles.listHeader}>{item.second}</Text>
                                    <Text style={styles.listHeader}>{item.third}</Text>
                                </View>
                            </ListItem>
                        )}
                    />
                </List>
                <View style={styles.title}>
                    <H3>Create an announcement</H3>
                </View>
                <Text style={styles.dropdownLabel}>I want to..</Text>
                {this.dropDownRenderer(adTypeData, 'adType')}
                <View>
                    <Text style={styles.instructions}>
                        If you want to sell a cryptocurrency, make sure that your Bitzlato wallet has a cryptocurrency
                    </Text>
                </View>
                <Text style={styles.dropdownLabel}>Cryptocurrency</Text>
                {this.dropDownRenderer(cryptocurrencies, 'crypto')}
                <Text style={styles.dropdownLabel}>Currency</Text>
                {this.dropDownRenderer(currencies,'currencies')}
                <Text style={styles.dropdownLabel}>Payment method. Choose a currency</Text>
                {this.dropDownRenderer(paymethods,'payment')}
               
                <Form>
                   
                    <Item stackedLabel>
                        <Label>The percentage of the exchange rate</Label>
                        <Input 
                        onChangeText={this.onChangePercentage.bind(this)}/>
                        {/* <Icon name="percentage" /> */}
                    </Item>
                    <Item stackedLabel>
                        <Label>exchange rate value</Label>
                        <Input 
                        onChangeText={this.onChangeManualPercentage.bind(this)}/>
                    </Item>
                    <Item stackedLabel>
                        <Label>minimum amount in currency</Label>
                        <Input 
                          onChangeText={this.onChangeMinAmount.bind(this)}/>
                    </Item>
                    <Item stackedLabel>
                        <Label>maximum amount in currency</Label>
                        <Input 
                          onChangeText={this.onChangeMaxAmount.bind(this)}/>
                    </Item>
                    <Item stackedLabel>
                        <Label>terms of trade</Label>
                        <Input multiline 
                          onChangeText={this.onChangeTerms.bind(this)}/>
                    </Item>
                    <Item stackedLabel>
                        <Label>Requisites for money transfer. Only for selling cryptocurrency</Label>
                        <Input multiline 
                          onChangeText={this.onChangeRequisite.bind(this)}/>
                    </Item>
                    <Button style={styles.createButton} full
                        onPress={() => this.hitCreateAdvertApi()} >
                        <Text style={{color:"#fff",fontSize:16}}>Create</Text>
                    </Button>
                </Form>
            </ScrollView>
        );
    }
}

const styles = {
    listHeader: {
        flex: 1,
        color: 'black',
        fontSize: 16
    },
    headerContainer: {
        flexDirection: 'row',
        marginTop: 8
    },
    title: {
        padding: 20
    },
    pickerStyle: {
        margin: 8,
        width:200,
        borderColor: 'gray', 
        borderWidth: 1,
         borderRadius: 2
    },
    instructions: { padding: 10, color: '#000' },
    dropdownLabel:{
        marginLeft:12
    },
    createButton:{
        backgroundColor:"#233070",
        borderRadius:6,
        height:35,
        margin:8
    }
};


const mapStateToProps=(state)=>{
    return {
        refs:state.refs,
        advert:state.adverts,
        state
    }
}

const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({createAdvert, loadDsaPaymethodsRef},dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AddAnnouncement);