import React from "react";
import { Text, View, Button, Alert, AsyncStorage } from "react-native";
import {StackActions, NavigationActions} from 'react-navigation'
import Auth0 from 'react-native-auth0';
import {whoami} from '../../redux/actions/accessToken';
import {store}from "../../../App"
import {connect} from "react-redux";
import {bindActionCreators}from "redux";
import Singleton from "../../utils/Singleton";


const auth0 = new Auth0(
{
    domain: 'bitzlato-dev.auth0.com',
    clientId: 'OL926gD0Zha6h80uJx4TVhJLMKrJemjb',
    scope: 'openid profile email'
});

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            moved:false
        }
    }

    static navigationOptions = {
        title: "Login",
    };

    
    componentDidMount(){
      this.authenticate()
    }

    callApi(token){
        store.dispatch(whoami(token))
    }

    authenticate(){
        auth0
        .webAuth
        .authorize({scope: 'openid profile email', audience: 'https://bitzlato-dev.auth0.com/userinfo'})
        .then(credentials =>{
          console.log(credentials)
          // Successfully authenticated
          // Store the accessToken     
         const type= credentials.tokenType;
         const token= credentials.idToken;
         const user_id= credentials.accessToken;
         console.log(type+' '+token)
         this.callApi(token)
          AsyncStorage.setItem('TOKEN', token);
        this.setState({moved:true})
        }
        )
        .catch(error => 
            console.log(error));
    }

    receivedProps(props){
        console.log('props::', props)
        if(this.state.moved){
        if(!Object.keys(props.userId.accessToken).length == 0){
            if((props.userId.accessToken.data) !== null){
                if(!Object.keys(props.userId.accessToken.data).length == 0){
                    const userid= props.userId.accessToken.data.userId
                    let singleton = Singleton.getInstance();
                    singleton.setUserId(userid);
                    AsyncStorage.setItem('USERID', String(userid));
                    const resetAction = StackActions.reset({
                     index: 0,
                     actions: [NavigationActions.navigate({ routeName: 'Dashboard' })],
                     });
                     this.props.navigation.dispatch(resetAction)
                    this.setState({moved:false})
                    }     
            }
        } 
    }
    }

    render() {
      this.receivedProps(this.props);

       return (
            <View style={{ flex: 1 , marginBottom:8}}>

            </View>)
    }
}

const mapStateToProps=(state)=>{
    return {
        userId:state
    }
}

const mapDispatchToProps=(dispatch)=>{
    return bindActionCreators({whoami},dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);