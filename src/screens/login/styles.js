export default  styles = {
    headerContainer:{
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'grey',
        height: 30,
        justifyContent: 'center',
        backgroundColor:"#fcf1ee"
    },
    baseText: {
      fontFamily: 'Cochin',
    },
    titleText: {
      fontSize: 25,
      fontWeight: 'bold',
    },
    logoStyle:{
        height:65,
        width:55
    }
  }