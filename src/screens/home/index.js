import React from "react";
import { Text, View, Picker, Switch, Button, FlatList, ScrollView, Image,TouchableOpacity, Platform } from "react-native";
import IOSPicker from 'react-native-ios-picker';

export default class Home extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            apiData:[],
            activeSwitch:false,
            verifiedSwitch:true,
            type:'I want to..',
            data:[{label:'I want to..', value:'I want to..'},
            {label:'to sell', value:'selling'},
            {label:'buy', value:'purchase'}],
            
            currency1:'BTC',
            dataCurrency:[{label:'Btc', value:'BTC'},
            {label:'DOGE', value:'DOGE'},
            {label:'DASH', value:'DASH'},
            {label:'LTC', value:'LTC'},
            {label:'Bch', value:'BCH'},
            {label:'Eth', value:'ETH'}],
            
            currency2:'RUB',
            dataCurrency2:[{label:'RUB', value:'RUB'},
            {label:'USD', value:'USD'},
            {label:'BYN', value:'BYN'},
            {label:'MXN', value:'MXN'},
            {label:'Aud', value:'Aud'},
            {label:'CAD', value:'CAD'}],

            payment:'Payment Method',
            paymentMethod:[{label:'Payment Method', value:'Payment Method'},
                {label:'QIWI', value:'qiwi'},
            {label:'Sberbank', value:'sberbank'},
            {label:'Tinkoff', value:'tinkoff'},
            {label:'Yandex Money', value:'yandex_money'},
            {label:'Alfabank', value:'alfabank'},
            {label:'From Card to Card', value:'card'},
            {label:'VISA', value:'visa'},
            {label:'Mastercard', value:'mastercard'}],

            size:[{label:'five', value:5},{label:"ten", value:10},{label:"fifteen", value:15}],
            selectedLimit:5,
            skip:5,
            skipPrevious:1,
            total:'256'
        }
    }

    componentDidMount(){  
        const {type, currency1, currency2, payment, verifiedSwitch}=this.state

        if(this.props.navigation.state.params !== undefined){
            const {userId}= this.props.navigation.state.params;
            url= `https://bitzlato.com/api/p2p/public/exchange/dsa/?limit=${this.state.selectedLimit}&skip=${this.state.skip}&isOwnerVerificated=${verifiedSwitch}`            
        }else{
        url= `https://bitzlato.com/api/p2p/public/exchange/dsa/?limit=${this.state.selectedLimit}&skip=${this.state.skip}&isOwnerVerificated=${verifiedSwitch}`
        }
        console.log('url:', url)

        this.hitTableApi(url)
    }

     hitTableApi(url){       
      console.log('url',url)
        fetch(url)
        .then(res=>res.json())
        .then(res=>{
            this.setState({apiData:res.data, total:res.total});
        }).catch((eee)=>{
            console.log("error",eee)
        })
     }

selectedValue(value){
    if(value === 'type'){
       return this.state.type
    }else if(value === 'crypto'){
       return this.state.currency1
    }else if(value === 'currency'){
       return this.state.currency2
    }else if(value === 'payment'){
       return this.state.payment
    }else if(value ==='limit'){
        return this.state.selectedLimit
    }
}
     
     renderPicker(data, value){
        if(Platform.OS === 'ios'){
            return  <View style={{ borderColor: 'gray', borderWidth: 1,  margin: 8, borderRadius: 2 }}>
            <IOSPicker 
                mode='modal'
                style={{ height: 40 }}
                selectedValue={this.selectedValue(value)}
                onValueChange={(itemValue, itemIndex) =>
                    this.typeSelected(itemValue, value)
                }>
                {this.renderPickerItem(data)}
                </IOSPicker> 
                </View>;
        }else{
            return <View style={{ borderColor: 'gray', borderWidth: 1, height: 40, margin: 8, borderRadius: 4 }}>
            <Picker style={{ height: 40 }}
            selectedValue={this.selectedValue(value)}
            onValueChange={(itemValue, itemIndex) =>
                this.typeSelected(itemValue,value)
                }>
                {this.renderPickerItem(data)}
            </Picker> 
            </View> ;
       }
    }

    renderSizePicker(data, value){
        if(Platform.OS === 'ios'){
            return  <View style={{  alignItems:'flex-end', flex:1}}>
                <View style={{width:100,borderColor: 'gray', borderWidth: 1, borderRadius: 2}}>
                <IOSPicker 
                mode='modal'
                style={{ height: 40 , width:80}}
                selectedValue={this.selectedValue(value)}
                onValueChange={(itemValue, itemIndex) =>
                    this.typeSelected(itemValue, value)
                }>
                {this.renderPickerItem(data)}
                </IOSPicker> 
                </View>
                </View>;
        }else{
            return <View style={{flex:1, alignItems:'flex-end'}}> 
            <Picker style={{ height: 40 , width: 130}}
            selectedValue={this.selectedValue(value)}
            onValueChange={(itemValue, itemIndex) =>
                this.typeSelected(itemValue,value)
                }>
                {this.renderPickerItem(data)}
            </Picker> 
            </View>;
        }
    }

    renderPickerItem(data){
    return data.map((item, key) => {
            return ( 
                        <Picker.Item key={key} label={item.label} value={item.value} />
                    );
        })
    }

    onBackPressed(){
        const skipValue=this.state.skip - this.state.selectedLimit
        var value=this.state.skipPrevious-this.state.selectedLimit
        if(String(value).includes("-")){
            value=1;
            this.setState({skipPrevious:value})
        }else if(String(value).includes("0")){
            value=1;
            this.setState({skip:skipValue, skipPrevious:value})
        }
        else{
            this.setState({skip:skipValue, skipPrevious:value})
        }

        const {type, currency1, currency2, payment, verifiedSwitch}=this.state

        if(this.props.navigation.state.params !== undefined){
            const {userId}= this.props.navigation.state.params;
            url= `https://bitzlato.com/api/p2p/public/exchange/dsa/?type=${type}&cryptocurrency=${currency1}&currency=${currency2}&limit=${this.state.selectedLimit}&skip=${skipValue}&isOwnerVerificated=${verifiedSwitch}`            
        }else{
            url= `https://bitzlato.com/api/p2p/public/exchange/dsa/?type=${type}&cryptocurrency=${currency1}&currency=${currency2}&limit=${this.state.selectedLimit}&skip=${skipValue}&isOwnerVerificated=${verifiedSwitch}`
        }

        this.hitTableApi(url)
        
    }

    onForwardPressed(){
        const skipValue=this.state.skip + this.state.selectedLimit
       
        const {type, currency1, currency2, payment, verifiedSwitch}=this.state

        if(this.props.navigation.state.params !== undefined){
            const {userId}= this.props.navigation.state.params;
            url= `https://bitzlato.com/api/p2p/public/exchange/dsa/?type=${type}&cryptocurrency=${currency1}&currency=${currency2}&limit=${this.state.selectedLimit}&skip=${skipValue}&isOwnerVerificated=${verifiedSwitch}`            
        }else{
            url= `https://bitzlato.com/api/p2p/public/exchange/dsa/?type=${type}&cryptocurrency=${currency1}&currency=${currency2}&limit=${this.state.selectedLimit}&skip=${skipValue}&isOwnerVerificated=${verifiedSwitch}`
        }

        this.hitTableApi(url)
        this.setState({skip:skipValue, skipPrevious:this.state.skip})
    }

    render() {

        return (
            <ScrollView style={{ flex: 1 }}>
         
                <View style={{ flexDirection: 'column', flex: 1, flexWrap: 'wrap' }}>

                    <View style={{ margin: 8 }}>
                        <Text style={{ fontSize: 20, color:'black' }}>
                            Buy and Sell cryptocurrency intantly.
                        </Text>
                    </View>
                    
                    {this.renderPicker(this.state.data, 'type')}

                    {this.renderPicker(this.state.dataCurrency, 'crypto')}
                     
                     {this.renderPicker(this.state.dataCurrency2, 'currency')}
                       
                    {this.renderPicker(this.state.paymentMethod, 'payment')}

                    <View style={{ margin: 8, alignItems: 'flex-start', flexDirection: 'row' }}>
                        <Switch
                            value={this.state.activeSwitch}
                            onValueChange={(val) =>this.setState({activeSwitch:val})}
                        />
                        <Text style={{ color: 'black', marginLeft: 8 }}>
                            Only active
                        </Text>
                    </View>

                    <View style={{ margin: 8, alignItems: 'flex-start', flexDirection: 'row' }}>
                        <Switch
                            value={this.state.verifiedSwitch}
                            onValueChange={(val) =>this.setState({verifiedSwitch:val})}
                        />
                        <Text style={{ color: 'black', marginLeft: 8 }}>
                            Verified User
                        </Text>

                    </View>

                    <View style={{ margin: 8 }}>
                        <Button
                            onPress={() => this.searchPressed()}
                            title="SEARCH"
                            color="#233070"
                        />
                    </View>

                    <View style={{
                        margin: 8, elevation: 1,
                        flexDirection: 'column',
                        borderRadius: 3,
                        borderColor: '#000',
                        padding: 10
                    }}>

                        <View style={{ flexDirection: 'row', marginTop: 8 }}>
                            <Text style={{ flex: 1, color: 'black', fontSize: 16 }}>
                                Trader
                        </Text>
                            <Text style={{ flex: 1, color: 'black', fontSize: 16 }}>
                                Course({this.state.currency2}/{this.state.currency1})
                        </Text>
                            <Text style={{ flex: 1, color: 'black', fontSize: 16 }}>
                                Limits({this.state.currency2})
                        </Text>

                        </View>
                        <View style={{ height: 0.2, marginTop: 2, backgroundColor: 'black' }} />

                        <FlatList
                            style={{flex:1}}
                            data={this.state.apiData}
                            keyExtractor={item => item.key}
                            renderItem={({ item }) =>
                                <View style={{ marginTop: 8 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ flex: 1, color: 'green', fontSize: 16 }}>
                                            {item.owner}
                                        </Text>
                                        <Text style={{ flex: 1, color: 'black', fontSize: 16 }}>
                                            {item.rate}
                                        </Text>
                                        <Text style={{ flex: 1, color: 'black', fontSize: 16 }}>
                                            {item.min}-{item.max}
                                        </Text>
                                    </View>

                                    <View style={{ height: 0.2, marginTop: 2, backgroundColor: 'black' }} />
                                </View>
                            }
                        />

                        <View style={{ flexDirection: 'row', marginTop: 2, alignItems:'center' }}>
                                                       
                            {this.renderSizePicker(this.state.size, 'limit')}
                         
                            <Text style={{flex:0.5}}>
                                {this.state.skipPrevious}-{this.state.skip} of {this.state.total}
                            </Text>
                            
                            <View style={{flexDirection:'row', flex:0.3, alignItems:'center'}}>
                                
                                <TouchableOpacity onPress={this.onBackPressed.bind(this)}>
                                    <Image
                                    style={{height:25, width:25}}
                                    source={require('../../assets/images/left.png')}
                                    />
                                </TouchableOpacity>

                                <TouchableOpacity onPress={this.onForwardPressed.bind(this)}>
                                    <Image
                                    style={{height:25, width:25, marginLeft:4}}                              
                                    source={require('../../assets/images/right.png')}
                                    />
                                </TouchableOpacity>
                            </View>

                        </View>                        

                    </View>

                </View>
            </ScrollView>
        );
    }

    searchPressed(){
        const {type, currency1, currency2, payment, verifiedSwitch}=this.state

        if(this.props.navigation.state.params !== undefined){
            const {userId}= this.props.navigation.state.params;
            url= `https://bitzlato.com/api/p2p/public/exchange/dsa/?type=${type}&cryptocurrency=${currency1}&currency=${currency2}&limit=${this.state.selectedLimit}&skip=${this.state.skip}&isOwnerVerificated=${verifiedSwitch}`            
        }else{
            url= `https://bitzlato.com/api/p2p/public/exchange/dsa/?type=${type}&cryptocurrency=${currency1}&currency=${currency2}&limit=${this.state.selectedLimit}&skip=${this.state.skip}&isOwnerVerificated=${verifiedSwitch}`
        }

        this.hitTableApi(url)
    }

    typeSelected = (itemValue,value) => {
        if(value === 'type'){
            this.setState({ type: itemValue })
        }else if(value === 'crypto'){
            this.setState({ currency1: itemValue })
        }else if(value === 'currency'){
            this.setState({ currency2: itemValue })
        }else if(value === 'payment'){
            this.setState({ payment: itemValue })
        }else if(value === 'limit'){
            this.setState({selectedLimit: itemValue})
            this.hitTableApi(itemValue,0)
        }
    }
}