// STATS
export const LOAD_LAST_DEAL = 'stats/LAST_DEAL';

// MAINTENANCE
export const MAINTENANCE = 'maintenance/ON';

// PROFILE
export const LOAD_PROFILE_SETTINGS = 'profile/LOAD';
export const LOAD_PROFILE_REFERRAL_LINKS = 'profile/REFERRAL_LINKS';
export const LOAD_PROFILE_RATE_SOURCES = 'profile/RATE_SOURCES';
export const CLEAR_PROFILE = 'profile/CLEAR';

// USER
export const LOAD_USER = 'user/LOAD_USER';
export const LOAD_USER_CHAT_MESSAGES = 'user/chat/LOAD_MESSAGES';
export const NEW_USER_CHAT_MESSAGE = 'user/chat/NEW_MESSAGE';

// PROFILE

export const LOAD_PROFILE = 'profile/LOAD';
export const UPDATE_PROFILE = 'profile/UPDATE';

// TIMEZONE

export const SET_TIMEZONE = 'timezone/set';

// WALLETS

export const ALL_WALLETS = 'wallets/ALL';
export const GET_WALLET_INFO = 'wallets/ONE';
export const ALL_REPORTS = 'wallets/reports/ALL';
export const GET_REPORT = 'wallets/reports/ONE';
export const LOAD_VOUCHERS = 'reports/wallets/vouchers/LIST';
export const ADD_VOUCHER = 'reports/wallets/vouchers/ADD';
export const REMOVE_VOUCHER = 'reports/wallets/vouchers/DELETE';
export const LOAD_TRANSACTIONS = 'reports/wallets/transactions/LIST';
export const UPDATE_TRANSACTION = 'reports/wallets/transactions/UPDATE';
export const LOAD_WALLET_STATS = 'wallets/STATS';
export const LOAD_WITHDRAWAL_INFO = 'wallets/withdrawal/INFO';

// ADVERTS

export const LOAD_ADVERTS = 'profile/adverts/LIST';
export const LOAD_ONE_ADVERT = 'profile/adverts/GET';
export const REMOVE_ADVERT = 'profile/adverts/REMOVE';
export const LOAD_TRADE_STATUS = 'profile/adverts/TRADE_STATUS';

// TRADES

export const LOAD_TRADES = 'trades/GRID';
export const LOAD_ONE_TRADE = 'trades/GET';
export const ESTIMATE_TRADE = 'trades/ESTIMATE';
export const PROMPT_TRADE_DETAILS = 'trades/PROMPT_DETAILS';
export const CANCEL_PROMPT_TRADE_DETAILS = 'trades/CANCEL_PROMPT_DETAILS';

// EXCHANGE

export const LOAD_EXCHANGE_MIX_ADVERTS = 'exchange/MIX_ADVERTS';
export const LOAD_EXCHANGE_BUY = 'exchange/BUY';
export const LOAD_EXCHANGE_SELL = 'exchange/SELL';
export const LOAD_EXCHANGE_ADVERT = 'exchange/LOAD_ADVERT';
export const SAVE_LAST_FILTER = 'exchange/LAST_FILTER';

// CHAT

export const LOAD_TRADE_CHAT_MESSAGES = 'trade/chat/LOAD_MESSAGES';
export const NEW_TRADE_CHAT_MESSAGE = 'trade/chat/NEW_MESSAGE';
export const LOAD_TRADE_ADMIN_CHAT_MESSAGES = 'trade/admin-chat/LOAD_MESSAGES';
export const NEW_TRADE_ADMIN_CHAT_MESSAGE = 'trade/admin-chat/NEW_MESSAGE';

// REFS

export const LOAD_CURRENCIES = 'refs/CURRENCIES';
export const LOAD_CRYPTOCURRENCIES = 'refs/CRYPTOCURRENCIES';
export const LOAD_PAYMETHODS = 'refs/PAYMETHODS';
export const LOAD_FEEDBACK_RATINGS = 'refs/FEEDBACKS';
export const LOAD_RATE_SOURCES = 'refs/RATE_SOURCES';
export const LOAD_REPORTS = 'refs/REPORTS';
export const LOAD_DSAPAYMETHODS = 'refs/DSAPAYMETHODS';

// ALERTS

export const ALERT = 'alert/ALERT';
export const CLEAR_ALERT = 'alert/CLEAR';

// SNACKBARS

export const ADD_SNACKBAR = 'snackbas/ADD';
export const REMOVE_SNACKBAR = 'snackbas/REMOVE';

// NOTIFICATIONS

export const LOAD_NOTIFICATIONS = 'notifications/ALL';
export const NEW_NOTIFICATION = 'notifications/NEW';
export const MARK_NOTIFICATION_AS_READED = 'notifications/READED';
export const SHOW_NOTIFICATIONS = 'notifications/SHOW';
export const HIDE_NOTIFICATIONS = 'notifications/HIDE';

// SAFE MODE

export const OPEN_SAFEMODE = 'safemode/open';
export const CLOSE_SAFEMODE = 'safemode/close';
export const NEXT_STEP_SAFEMODE = 'safemode/next';
export const SET_VALUE_SAFEMODE = 'safemode/value';
export const ERROR_SAFEMODE = 'safemode/error';

// MARKET

export const LIST_PAIRS = 'market/pairs/list';
export const LOAD_PAIR = 'market/pairs/load';
export const LOAD_ORDERBOOK = 'market/orderbook/load';
export const LOAD_PERSONAL_ORDERS = 'market/personalOrders/load';
export const LOAD_MARKET_TRADES = 'market/trades/load';
export const LOAD_MY_ORDERS = 'market/myOrders/load';
export const SWITCH_PAIR = 'market/switchPair';
export const EVENT_NEW_ORDER = 'market/event/newOrder';
export const EVENT_NEW_ORDER_PERSONAL = 'market/eventPersonal/newOrder';
export const EVENT_ORDER_REMOVED = 'market/event/orderRemoved';
export const EVENT_ORDER_REMOVED_PERSONAL = 'market/eventPersonal/orderRemoved';
export const EVENT_ORDER_CHANGED = 'market/event/orderChanged';
export const EVENT_ORDER_CHANGED_PERSONAL = 'market/eventPersonal/orderChanged';
export const EVENT_NEW_TRADES = 'market/event/newTrades';
export const LOAD_CANDLESTICK_START = 'market/candlesticks/loadStart';
export const LOAD_CANDLESTICK_END = 'market/candlesticks/loadEnd';
export const SET_MYORDERS_FILTER = 'market/myOrders/filter';
export const SET_MYORDERS_PAGE = 'market/myOrders/page';
export const SET_MYORDERS_LIMIT = 'market/myOrders/limit';
export const LOAD_PERSONAL_TRADES = 'market/personalTrades/load';
export const SET_PERSONAL_TRADES_FILTER = 'market/personalTrades/filter';
export const SET_PERSONAL_TRADES_PAGE = 'market/personalTrades/page';
export const SET_PERSONAL_TRADES_LIMIT = 'market/personalTrades/limit';

// USER INFO

export const LOAD_USER_INFO = 'userInfo/load';

// ACCESS TOKEN

export const OPEN_ACCESS_TOKEN = 'accessToken/open';
export const CLOSE_ACCESS_TOKEN = 'accessToken/close';
export const GET_ACCESS_TOKEN = 'accessToken/get';
export const LOAD_USERID= 'accesstoken/user'

// SIGNALS

export const LOAD_SIGNALS = 'signals/load';
