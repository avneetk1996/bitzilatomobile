import { ADD_SNACKBAR, REMOVE_SNACKBAR } from '../constants';

export default (state = [], action) => {
    switch (action.type) {
        case ADD_SNACKBAR:
            return [...state, { ...action.notification }];
        case REMOVE_SNACKBAR:
            return state.filter(n => n.key !== action.key);
        default:
            return state;
    }
};
