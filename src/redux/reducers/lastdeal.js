import { LOAD_LAST_DEAL } from '../constants';

export default function lastdeal(state = null, action = {}) {
    switch (action.type) {
        case LOAD_LAST_DEAL:
            return action.lastDeal;
        default:
            return state;
    }
}
