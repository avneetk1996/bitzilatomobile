import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { connectRouter } from 'connected-react-router';
import {navReducer} from "../../router/Router"

import accessToken from './accessToken';
import adverts from './adverts';
import alerts from './alerts';
import exchange from './exchange';
import lastdeal from './lastdeal';
import maintenance from './maintenance';
import market from './market';
import notifications from './notifications';
import profile from './profile';
import refs from './refs';
import safemode from './safemode';
import signals from './signals';
import snackbars from './snackbars';
import timezone from './timezone';
import trades from './trades';
import userInfo from './userInfo';
import users from './users';
import wallets from './wallets';

export const client = history =>
    combineReducers({
        navReducer,
        accessToken,
        adverts,
        alerts,
        auth: (state = {}) => state,
        backendUrl: (state = '') => state,
        exchange,
        form: formReducer,
        lastdeal,
        maintenance,
        market,
        notifications,
        profile,
        refs,
        router: connectRouter(history),
        safemode,
        signals,
        snackbars,
        timezone,
        trades,
        userInfo,
        users,
        wallets,
        
    });

export const ssr = () =>
    combineReducers({
        navReducer,
        accessToken,
        adverts,
        alerts,
        auth: (state = {}) => state,
        backendUrl: (state = '') => state,
        cookies: (state = []) => state,
        exchange,
        form: formReducer,
        maintenance,
        market,
        notifications,
        profile,
        refs,
        safemode,
        signals,
        snackbars,
        timezone,
        trades,
        userInfo,
        users,
        wallets,
        lastdeal
    });

export const widget = () =>
    combineReducers({
        backendUrl: (state = '') => state,
        refs,
        form: formReducer
    });
