import _ from 'lodash';

import {
    ALL_WALLETS,
    GET_WALLET_INFO,
    LOAD_VOUCHERS,
    ADD_VOUCHER,
    REMOVE_VOUCHER,
    LOAD_TRANSACTIONS,
    UPDATE_TRANSACTION,
    LOAD_WALLET_STATS,
    LOAD_WITHDRAWAL_INFO
} from '../constants';

export default function wallets(state = {}, action = {}) {
    switch (action.type) {
        case ALL_WALLETS: {
            return _.transform(
                action.wallets,
                (result, { cryptocurrency }) => {
                    if (!_.has(result, cryptocurrency)) {
                        result[cryptocurrency] = {};
                    }
                },
                { ...state }
            );
        }
        case GET_WALLET_INFO: {
            const wallet = _.omit(action.wallet, 'worth');

            if (action.wallet.worth) {
                _.set(wallet, ['worth', action.wallet.worth.currency], action.wallet.worth);
            }

            if (action.rate) {
                wallet.rateSource = {
                    ...wallet.rates,
                    [action.rate.currency]: _.pick(action.rate, ['url', 'rate'])
                };
            }

            const res = {
                ...state,
                [action.wallet.cryptocurrency]: {
                    ...state[action.wallet.cryptocurrency],
                    ...wallet
                }
            };

            return res;
        }
        case LOAD_WALLET_STATS: {
            return _.merge({}, state, {
                [action.cryptocurrency]: {
                    stats: action.stats
                }
            });
        }
        case LOAD_WITHDRAWAL_INFO:
            return _.merge({}, state, {
                [action.cryptocurrency]: {
                    info: action.info
                }
            });
        case LOAD_VOUCHERS:
            return {
                ...state,
                [action.cryptocurrency]: {
                    ...state[action.cryptocurrency],
                    vouchers: {
                        filter: action.filter,
                        items: action.vouchers
                    }
                }
            };
        case ADD_VOUCHER: {
            const add_r = _.cloneDeep(state);
            add_r[action.voucher.cryptocurrency.code].vouchers.items.data.push(action.voucher);
            add_r[action.voucher.cryptocurrency.code].vouchers.items.total += 1;
            return add_r;
        }
        case REMOVE_VOUCHER: {
            const rem_r = _.cloneDeep(state);
            _.forEach(rem_r, (v, k) => {
                const rem_res = [];
                if (rem_r[k].vouchers) {
                    rem_r[k].vouchers.items.data.forEach(e => {
                        if (e.deepLinkCode !== action.code) {
                            rem_res.push(e);
                        } else {
                            rem_r[k].vouchers.items.total -= 1;
                        }
                    });
                    rem_r[k].vouchers.items.data = rem_res;
                }
            });

            return rem_r;
        }
        case LOAD_TRANSACTIONS:
            return {
                ...state,
                [action.cryptocurrency]: {
                    ...state[action.cryptocurrency],
                    transactions: {
                        filter: action.filter,
                        total: action.total,
                        items: action.transactions
                    }
                }
            };
        case UPDATE_TRANSACTION:
            return {
                ...state,
                [action.tx.cryptocurrency.code]: {
                    ...state[action.tx.cryptocurrency.code],
                    transactions: {
                        ..._.get(state, [action.tx.cryptocurrency.code, 'transactions'], {}),
                        items: _.map(
                            _.get(state, [action.tx.cryptocurrency.code, 'transactions', 'items'], []),
                            item => {
                                return action.tx.id === item.id ? action.tx : item;
                            }
                        )
                    }
                }
            };
        default:
            return state;
    }
}
