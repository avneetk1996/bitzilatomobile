/* eslint-env jest */
import { LIST_PAIRS } from '../constants';
import { pairs, orderbook } from './market';

describe('market reducer', () => {
    describe('pairs reducer', () => {
        it('should handle LIST_PAIRS', () => {
            expect(
                pairs(null, {
                    type: LIST_PAIRS,
                    pairs: [{ id: 'BTC-USD' }]
                })
            ).toEqual([{ id: 'BTC-USD', base: 'BTC', quote: 'USD' }]);
        });
    });

    describe('orderbook reducer', () => {
        it('newOrder sort bug', () => {
            const init = {
                'ETH-BTC-ask': [
                    { id: 14, price: 2, offerType: 'ask', pair: 'ETH-BTC', amount: 1 },
                    { id: 15, userId: 2, offerType: 'ask', amount: '1', price: '3' }
                ]
            };

            const event = {
                type: 'market/event/newOrder',
                event: { pair: 'ETH-BTC', data: { id: 16, userId: 2, offerType: 'ask', amount: '1', price: '1' } }
            };

            const shouldBe = {
                'ETH-BTC-ask': [
                    { id: 16, userId: 2, offerType: 'ask', amount: '1', price: '1' },
                    { id: 14, price: 2, offerType: 'ask', pair: 'ETH-BTC', amount: 1 },
                    { id: 15, userId: 2, offerType: 'ask', amount: '1', price: '3' },
                ]
            };

            expect(orderbook(init, event)).toEqual(shouldBe);
        });
    });

    describe('personalOrders reducer', () => {
    });
});
