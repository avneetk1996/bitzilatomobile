import { combineReducers } from 'redux';

import {
    LIST_PAIRS,
    LOAD_PAIR,
    LOAD_ORDERBOOK,
    LOAD_PERSONAL_ORDERS,
    LOAD_MARKET_TRADES,
    LOAD_MY_ORDERS,
    SWITCH_PAIR,
    EVENT_NEW_ORDER,
    EVENT_NEW_ORDER_PERSONAL,
    EVENT_ORDER_REMOVED,
    EVENT_ORDER_REMOVED_PERSONAL,
    EVENT_ORDER_CHANGED,
    EVENT_ORDER_CHANGED_PERSONAL,
    EVENT_NEW_TRADES,
    LOAD_CANDLESTICK_START,
    LOAD_CANDLESTICK_END,
    SET_MYORDERS_FILTER,
    SET_MYORDERS_PAGE,
    SET_MYORDERS_LIMIT,
    LOAD_PERSONAL_TRADES,
    SET_PERSONAL_TRADES_FILTER,
    SET_PERSONAL_TRADES_PAGE,
    SET_PERSONAL_TRADES_LIMIT
} from '../constants';

export const pairs = (state = null, action) => {
    switch (action.type) {
        case LIST_PAIRS:
            return action.pairs.map(pair => {
                const [base, quote] = pair.id.split('-');
                return { ...pair, base, quote };
            });
        case LOAD_PAIR: {
            const list = state || [];
            return list.map(pair => {
                if (pair.id === action.pair) {
                    const [base, quote] = pair.id.split('-');
                    return { ...action.data, base, quote };
                } else {
                    return pair;
                }
            });
        }
        default:
            return state;
    }
};

export const pairsHash = (state = null, action) => {
    switch (action.type) {
        case LIST_PAIRS:
            return action.pairs.reduce((prev, pair) => {
                const [base, quote] = pair.id.split('-');
                prev[pair.id] = { ...pair, base, quote };
                return prev;
            }, {});
        case LOAD_PAIR: {
            const [base, quote] = action.pair.split('-');
            return {
                ...state,
                [action.pair]: { ...action.data, base, quote }
            };
        }
        default:
            return state;
    }
};

export const orderbook = (state = {}, action) => {
    switch (action.type) {
        case LOAD_ORDERBOOK:
            return { ...state, [`${action.pair}-${action.dir}`]: action.orderbook };
        case EVENT_NEW_ORDER: {
            const { pair, data } = action.event;
            const key = `${pair}-${data.offerType}`;
            if (key in state) {
                let orderbook = [...state[key], data];
                if (data.offerType === 'ask') {
                    orderbook = orderbook.sort((a, b) => -Number(b.price) + Number(a.price));
                } else {
                    orderbook = orderbook.sort((a, b) => +Number(b.price) - Number(a.price));
                }
                return { ...state, [key]: orderbook };
            } else {
                return state;
            }
        }
        case EVENT_ORDER_REMOVED: {
            const { data } = action.event;
            for (const key in state) {
                if (state[key].some(({ id }) => id === data.id)) {
                    state = { ...state, [key]: state[key].filter(({ id }) => id !== data.id) };
                }
            }
            return state;
        }
        case EVENT_ORDER_CHANGED: {
            const { data, pair } = action.event;
            const { id, offerType } = data;
            const key = `${pair}-${offerType}`;

            if (!(key in state)) return state;
            const index = state[key].findIndex(o => id == o.id);
            if (index === -1) return state;
            state[key][index] = data;
            state[key] = [...state[key]];
            return state;
        }
        default:
            return state;
    }
};

export const personalOrders = (state = {}, action) => {
    switch (action.type) {
        case LOAD_PERSONAL_ORDERS:
            return { ...state, [action.pair]: action.orders };
        case EVENT_NEW_ORDER_PERSONAL: {
            const { amount } = action.event.data;
            const order = { ...action.event.data, amount: { origin: amount, matched: 0, rest: amount } };
            const orders = [...state[action.event.pair], order].sort((a, b) => b.id - a.id);
            return { ...state, [action.event.pair]: orders };
        }
        case EVENT_ORDER_REMOVED_PERSONAL:
            for (const key in state) {
                if (state[key].some(({ id }) => id === action.event.data.id)) {
                    state = { ...state, [key]: state[key].filter(({ id }) => id !== action.event.data.id) };
                }
            }
            return state;
        case EVENT_ORDER_CHANGED_PERSONAL: {
            const { data, pair } = action.event;
            const { id } = data;
            if (!(pair in state)) return state;
            const index = state[pair].findIndex(o => id == o.id);
            if (index === -1) return state;
            state[pair][index] = { ...data, amount: { rest: data.amount } };
            state[pair] = [...state[pair]];
            return state;
        }
        default:
            return state;
    }
};

const TRADES_LIMIT = 100;

const trades = (state = {}, action) => {
    switch (action.type) {
        case LOAD_MARKET_TRADES: {
            let { trades } = action;
            if (trades.length > TRADES_LIMIT) trades = trades.slice(0, TRADES_LIMIT);
            return { ...state, [action.pair]: trades };
        }
        case EVENT_NEW_TRADES: {
            const { pair, data } = action.event;
            if (!(pair in state)) return state;

            let trades = [
                ...state[pair],
                ...data.map(t => ({ id: t.id, price: t.price, amount: t.amount, date: t.date, side: t.side }))
            ]
                .sort((a, b) => b.date - a.date)
                .filter((i, pos, array) => array.findIndex(({ id }) => i.id == id) === pos);

            if (trades.length > TRADES_LIMIT) trades = trades.slice(0, TRADES_LIMIT);

            return { ...state, [pair]: trades };
        }
        default:
            return state;
    }
};

const myorders = (state = { orders: [], filter: {}, limit: 10, total: null, skip: 0 }, action) => {
    switch (action.type) {
        case LOAD_MY_ORDERS:
            return { ...state, orders: action.orders, total: action.total };
        case EVENT_ORDER_REMOVED_PERSONAL:
            if (state.orders.some(({ id }) => id === action.event.data.id)) {
                state = { ...state, orders: state.orders.filter(({ id }) => id !== action.event.data.id) };
            }
            return state;
        case SET_MYORDERS_FILTER:
            return { ...state, filter: action.filter, skip: 0 };
        case SET_MYORDERS_PAGE:
            return { ...state, skip: action.page * state.limit };
        case SET_MYORDERS_LIMIT:
            return { ...state, limit: action.limit, skip: 0 };
        default:
            return state;
    }
};

const activePair = (state = null, action) => {
    switch (action.type) {
        case SWITCH_PAIR:
            return action.pair;
        default:
            return state;
    }
};

function rdfsa(arr, compareFunc) {
    let last;
    const res = [];
    arr.forEach(i => {
        if (!last || !compareFunc(i, last)) res.push(i);
        last = i;
    });
    return res;
}

const candlestick = (state = {}, action) => {
    switch (action.type) {
        case LOAD_CANDLESTICK_START: {
            const { pair, width, keysToFetch } = action.params;
            const key = `${pair}-${width}`;
            let arr = keysToFetch.map(i => ({ date: i }));
            if (key in state) {
                const data = state[key];
                arr = arr.filter(i => !data.some(({ date }) => date == i.date));
                arr = [...state[key], ...arr];
            }
            arr = arr.sort((a, b) => a.date - b.date);
            arr = rdfsa(arr, (a, b) => a.date == b.date);
            return { ...state, [key]: arr };
        }
        case LOAD_CANDLESTICK_END: {
            const { data, params } = action;
            const { pair, width } = params;
            const key = `${pair}-${width}`;
            let arr = state[key];
            arr = arr.filter(i => !data.some(({ date }) => date == i.date));
            arr = [...arr, ...data];
            arr = arr.sort((a, b) => a.date - b.date);
            arr = rdfsa(arr, (a, b) => a.date == b.date);
            return { ...state, [key]: arr };
        }
        default:
            return state;
    }
};

const personalTrades = (state = { trades: [], filter: {}, limit: 10, total: null, skip: 0 }, action) => {
    switch (action.type) {
        case LOAD_PERSONAL_TRADES:
            return { ...state, trades: action.trades, total: action.total };
        case SET_PERSONAL_TRADES_FILTER:
            return { ...state, filter: action.filter, skip: 0 };
        case SET_PERSONAL_TRADES_PAGE:
            return { ...state, skip: action.page * state.limit };
        case SET_PERSONAL_TRADES_LIMIT:
            return { ...state, limit: action.limit, skip: 0 };
        default:
            return state;
    }
};

export default combineReducers({
    pairs,
    pairsHash,
    orderbook,
    personalOrders,
    trades,
    personalTrades,
    myorders,
    activePair,
    candlestick
});
