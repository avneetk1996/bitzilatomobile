import { LOAD_SIGNALS } from '../constants';

export default function signals(state = {}, action = {}) {
    switch (action.type) {
        case LOAD_SIGNALS:
            return {
                ...state,
                [action.domain]: action.signals
            };
        default:
            return state;
    }
}
