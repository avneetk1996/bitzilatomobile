import { CLEAR_ALERT, ALERT } from '../constants';

export default function alerts(state = {}, action = {}) {
    switch (action.type) {
        case ALERT:
            return {
                ...action.message
            };
        case CLEAR_ALERT:
            return {};
        default:
            return state;
    }
}
