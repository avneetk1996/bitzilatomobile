import _ from 'lodash';

import { LOAD_ADVERTS, LOAD_ONE_ADVERT, REMOVE_ADVERT, LOAD_TRADE_STATUS } from '../constants';

export default function adverts(state = { all: [], items: {} }, action = {}) {
    switch (action.type) {
        case LOAD_ADVERTS:
            return {
                ...state,
                all: action.adverts
            };
        case LOAD_ONE_ADVERT:
            return {
                ...state,
                items: {
                    ...state.items,
                    [action.id]: action.advert
                }
            };
        case REMOVE_ADVERT:
            return {
                ...state,
                all: _.remove(state.all, item => item.id === action.id),
                items: _.omit(state.items, action.id)
            };
        case LOAD_TRADE_STATUS:
            return {
                ...state,
                tradeStatus: action.tradeStatus
            };
        default:
            return state;
    }
}
