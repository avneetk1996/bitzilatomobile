import { OPEN_ACCESS_TOKEN, CLOSE_ACCESS_TOKEN, GET_ACCESS_TOKEN, LOAD_USERID } from '../constants';
import { AsyncStorage } from "react-native";

const initial = { open: false, accessToken: null, expiryDate: null, data:null };

export default function accessToken(state = initial, action) {
    switch (action.type) {
        case OPEN_ACCESS_TOKEN:
            return { ...state, open: true };
        case CLOSE_ACCESS_TOKEN:
            return initial;
        case GET_ACCESS_TOKEN: {
            const { accessToken, expiryDate } = action.data;
            return { ...state, accessToken, expiryDate };
        }
        case LOAD_USERID:{
            data= action.whoami
            return { ...state, data};
        }
        default:
            return state;
    }
}
