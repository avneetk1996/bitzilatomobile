import { createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';

import { client, ssr, widget } from './reducers';
import { errorHandlerMiddleware } from './middleware';
import {navigationMiddleware}from "../router/Router";
import Logger from "redux-logger";

const composeEnhancers =
    typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
              // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
          })
        : compose;

export function createClientStore({ initialState, history, logout }) {
    return createStore(
        client(history),
        initialState,
        composeEnhancers(applyMiddleware(routerMiddleware(history), errorHandlerMiddleware(logout), thunk,navigationMiddleware,Logger))
    );
}

export function createSsrStore({ initialState, logout }) {
    return createStore(ssr(), initialState, composeEnhancers(applyMiddleware(errorHandlerMiddleware(logout), thunk,navigationMiddleware,Logger)));
}

export function createWidgetStore(initialState) {
    return createStore(widget(), initialState, applyMiddleware(thunk));
}

export * from './actions';
