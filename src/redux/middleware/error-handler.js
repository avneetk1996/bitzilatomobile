import { MAINTENANCE, ALERT, OPEN_SAFEMODE } from '../constants';

export const errorHandlerMiddleware = logout => () => next => async action => {
    try {
        await next(action);
    } catch (err) {
        console.log('error:',err)
        if (err.response && err.response.status === 401) {
            logout();
        } else if (err.response && err.response.status === 403) {
            next({
                type: ALERT,
                message: {
                    type: 'error',
                    text: 'forbidden'
                }
            });
        } else if (err.response && err.response.status === 416) {
            next({
                type: ALERT,
                message: {
                    type: 'error',
                    text: 'input_error'
                }
            });
        } else if (err.response && err.response.status === 462) {
            next({
                type: ALERT,
                message: {
                    type: 'error',
                    text: 'not_enough_funds'
                }
            });
        } else if (err.response && err.response.status === 464) {
            next({
                type: ALERT,
                message: {
                    type: 'error',
                    text: 'voucher_outdated'
                }
            });
        } else if (err.response && err.response.status === 466) {
            next({
                type: ALERT,
                message: {
                    type: 'error',
                    text: 'details_required'
                }
            });
        } else if (err.response && err.response.status === 468) {
            next({
                type: ALERT,
                message: {
                    type: 'error',
                    text: 'not_enough_rating'
                }
            });
        } else if (err.response && err.response.status === 469) {
            next({
                type: ALERT,
                message: {
                    type: 'error',
                    text: 'user_dont_accept_messages'
                }
            });
        } else if (err.response && err.response.status === 470) {
            next({
                type: ALERT,
                message: {
                    type: 'error',
                    text: 'already_have_trade'
                }
            });
        } else if (err.response && err.response.status === 471) {
            next({
                type: OPEN_SAFEMODE
            });
        } else if (err.response && err.response.status === 472) {
            next({
                type: ALERT,
                message: {
                    type: 'error',
                    text: 'We are currently under maintenance'
                }
            });
        } else if (err.response && err.response.status === 473) {
            next({
                type: ALERT,
                message: {
                    type: 'error',
                    text: 'Forbidden To Trade With Yourself'
                }
            });
        } else if (err.response && err.response.status === 475) {
            next({
                type: MAINTENANCE
            });
        } else if (err.response && err.response.status >= 500) {
            next({
                type: ALERT,
                message: {
                    type: 'error',
                    text: 'server_error'
                }
            });
        } else {
            throw err;
        }
    }
};
