import _ from 'lodash';
import { createApiClient } from './api';
import {
    LOAD_NOTIFICATIONS,
    NEW_NOTIFICATION,
    MARK_NOTIFICATION_AS_READED,
    SHOW_NOTIFICATIONS,
    HIDE_NOTIFICATIONS
} from '../constants';

export function loadNotifications() {
    return async function(dispatch, getState) {
        dispatch({
            type: LOAD_NOTIFICATIONS,
            items: await createApiClient(getState).notifications()
        });
    };
}

export function newNotification(id, type, data) {
    return {
        type: NEW_NOTIFICATION,
        id: id,
        name: type,
        data: data
    };
}

export function markNotificationAsReaded(id) {
    return async function(dispatch, getState) {
        await createApiClient(getState).markNotificationAsReaded(id);

        dispatch({
            type: MARK_NOTIFICATION_AS_READED,
            id: id
        });
    };
}

export function markAllNotificationsAsReaded() {
    return async function(dispatch, getState) {
        const apiClient = createApiClient(getState),
            state = getState();

        dispatch(hideNotifications());

        await Promise.all(
            _.map(_.get(state, 'notifications.items'), item => apiClient.markNotificationAsReaded(item.id))
        );
        await loadNotifications()(dispatch, getState);
    };
}

export function showNotifications(anchorEl) {
    return {
        type: SHOW_NOTIFICATIONS,
        anchorEl: anchorEl
    };
}

export function hideNotifications() {
    return {
        type: HIDE_NOTIFICATIONS
    };
}
