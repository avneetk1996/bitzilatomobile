import _ from 'lodash';
import { push } from 'connected-react-router';
import { createApiClient } from './api';
import {
    ALERT,
    ALL_WALLETS,
    GET_WALLET_INFO,
    LOAD_VOUCHERS,
    ADD_VOUCHER,
    REMOVE_VOUCHER,
    LOAD_TRANSACTIONS,
    UPDATE_TRANSACTION,
    LOAD_WALLET_STATS,
    LOAD_WITHDRAWAL_INFO
} from '../constants';
import { addSnackbar } from './snackbars';
import { WALLETS_URL } from '../../config';

export function loadWalletList() {
    return async function(dispatch, getState) {
        dispatch({
            type: ALL_WALLETS,
            wallets: await createApiClient(getState).wallets()
        });
    };
}

export function loadWalletInfo(cryptocurrency, currency) {
    return loadAllWalletsInfo([cryptocurrency], currency);
}

export function loadAllWalletsInfo(cryptocurrencies, currency) {
    return async function(dispatch, getState) {
        const state = getState();
        currency = currency || _.get(state, 'profile.settings.currency');
        const apiClient = createApiClient(getState);

        const items = await Promise.all(
            _.map(cryptocurrencies, cryptocurrency =>
                Promise.all([
                    apiClient.wallet(cryptocurrency, currency),
                    currency && apiClient.getRateSource(cryptocurrency, currency)
                ])
            )
        );

        for (const [wallet, rate] of items) {
            dispatch({
                type: GET_WALLET_INFO,
                wallet,
                rate: {
                    currency: currency,
                    url: rate.url,
                    rate: rate.rate
                }
            });
        }
    };
}

export function loadWithdrawInfo(cryptocurrency) {
    return async function(dispatch, getState) {
        dispatch({
            type: LOAD_WITHDRAWAL_INFO,
            cryptocurrency: cryptocurrency,
            info: await createApiClient(getState).withdrawInfo(cryptocurrency)
        });
    };
}

export function createWallet(cryptocurrency) {
    return async function(dispatch, getState) {
        await loadWalletInfo(cryptocurrency)(dispatch, getState);
        dispatch(push(`${WALLETS_URL}/wallets/${cryptocurrency}`));
    };
}

export function generateWalletAddress(cryptocurrency) {
    return async function(dispatch, getState) {
        await createApiClient(getState).generateWalletAddress(cryptocurrency);
        await loadWalletInfo(cryptocurrency)(dispatch, getState);
    };
}

export function withdraw(cryptocurrency, address, amount) {
    return async function(dispatch, getState) {
        try {
            await createApiClient(getState).withdraw(cryptocurrency, address, amount);

            dispatch(addSnackbar('Вывод средств запланирован успешно!'));

            await Promise.all([
                loadWalletInfo(cryptocurrency)(dispatch, getState),
                loadWithdrawInfo(cryptocurrency)(dispatch, getState)
            ]);
        } catch (err) {
            if (err.response && err.response.status == 472) {
                dispatch({
                    type: ALERT,
                    message: {
                        type: 'error',
                        text: 'withdrawal_unavailable'
                    }
                });
            } else {
                throw err;
            }
        }
    };
}

export function loadVoucherList(cryptocurrency, skip, limit) {
    return async function(dispatch, getState) {
        dispatch({
            type: LOAD_VOUCHERS,
            cryptocurrency: cryptocurrency,
            filter: { skip, limit },
            vouchers: await createApiClient(getState).vouchers({ cryptocurrency, skip, limit })
        });
    };
}

/**
 * Currency param can be crypto and fiat
 */
export function createVoucher(amount, cryptocurrency, currency, method) {
    return async function(dispatch, getState) {
        dispatch({
            type: ADD_VOUCHER,
            voucher: await createApiClient(getState).createVoucher(amount, cryptocurrency, currency, method)
        });
        dispatch(addSnackbar('Чек создан!'));
    };
}

export function cancelVoucher(code) {
    return async function(dispatch, getState) {
        await createApiClient(getState).cancelVoucher(code);
        dispatch({
            type: REMOVE_VOUCHER,
            code: code
        });
        dispatch(addSnackbar('Чек отменен!'));
    };
}

export function loadTransactions(cryptocurrency, skip, limit) {
    return async function(dispatch, getState) {
        const data = await createApiClient(getState).transactions({ cryptocurrency, skip, limit });
        dispatch({
            type: LOAD_TRANSACTIONS,
            cryptocurrency: cryptocurrency,
            filter: { skip, limit },
            total: data.total,
            transactions: data.data
        });
    };
}

export function commentTransaction(txid, comment) {
    return async function(dispatch, getState) {
        const tx = await createApiClient(getState).commentTransaction(txid, comment);

        await dispatch({
            type: UPDATE_TRANSACTION,
            tx
        });
    };
}

export function setRateSource(url, currency, cryptocurrency) {
    return async function(dispatch, getState) {
        await createApiClient(getState).setRateSource(url, currency, cryptocurrency);
        await loadWalletInfo(cryptocurrency)(dispatch, getState);
    };
}

export function loadDealStats() {
    return async function(dispatch, getState) {
        const apiClient = createApiClient(getState);

        const wallets = await apiClient.wallets();

        Promise.all(
            _.map(wallets, async ({ cryptocurrency }) => {
                dispatch({
                    type: LOAD_WALLET_STATS,
                    cryptocurrency: cryptocurrency,
                    stats: await apiClient.walletStats(cryptocurrency)
                });
            })
        );
    };
}

export function loadAllWalletsWithAllInfo() {
    return async function(dispatch, getState) {
        await dispatch(loadWalletList());
        const wallets = Object.keys(getState().wallets);
        await dispatch(loadAllWalletsInfo(wallets));
    };
}
