import { LOAD_USER, LOAD_USER_CHAT_MESSAGES, NEW_USER_CHAT_MESSAGE } from '../constants';
import { createApiClient } from './api';

export function loadUserInfo(publicName) {
    return async function(dispatch, getState) {
        dispatch({
            type: LOAD_USER,
            user: await createApiClient(getState).userinfo(publicName)
        });
    };
}

export function loadUserChat(publicName) {
    return async function(dispatch, getState) {
        dispatch({
            type: LOAD_USER_CHAT_MESSAGES,
            user: publicName,
            messages: await createApiClient(getState).loadUserMessages(publicName)
        });
    };
}

export function blockUser(publicName, flag) {
    return async function(dispatch, getState) {
        await createApiClient(getState).blockUser(publicName, flag);
        await loadUserInfo(publicName)(dispatch, getState);
    };
}

export function transferToUser(publicName, cryptocurrency, amount) {
    return async function(dispatch, getState) {
        await createApiClient(getState).transferToUser(publicName, cryptocurrency, amount);
    };
}

export function sendUserMessage(publicName, message) {
    return async function(dispatch, getState) {
        dispatch({
            type: NEW_USER_CHAT_MESSAGE,
            user: publicName,
            message: await createApiClient(getState).sendUserMessage(publicName, { message })
        });
    };
}
