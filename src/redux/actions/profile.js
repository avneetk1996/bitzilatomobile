import { createApiClient } from './api';
import { LOAD_PROFILE_SETTINGS, LOAD_PROFILE_REFERRAL_LINKS } from '../constants';

export function loadProfile() {
    return async function(dispatch, getState) {
        const apiClient = createApiClient(getState);
        const profile = await apiClient.profile();
        const stats = await apiClient.walletStats(profile.cryptocurrency || 'BTC', profile.currency || 'RUB');
        const features = await apiClient.features();

        dispatch({
            type: LOAD_PROFILE_SETTINGS,
            profile,
            stats,
            features
        });
    };
}

export function updateProfile(opts) {
    return async function(dispatch, getState) {
        dispatch({
            type: LOAD_PROFILE_SETTINGS,
            profile: await createApiClient(getState).updateProfile(opts)
        });
        await loadProfile()(dispatch, getState);
    };
}

export function mergeProfile(telegram) {
    return async function(dispatch, getState) {
        dispatch({
            type: LOAD_PROFILE_SETTINGS,
            profile: await createApiClient(getState).mergeProfile(telegram)
        });
    };
}

export function loadReferralLinks(cryptocurrency) {
    return async function(dispatch, getState) {
        const links = await createApiClient(getState).referralLinks();

        dispatch({
            type: LOAD_PROFILE_REFERRAL_LINKS,
            links
        });
    };
}
