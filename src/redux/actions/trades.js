import { push } from 'connected-react-router';
import {
    LOAD_TRADES,
    LOAD_ONE_TRADE,
    ESTIMATE_TRADE,
    PROMPT_TRADE_DETAILS,
    CANCEL_PROMPT_TRADE_DETAILS,
    LOAD_TRADE_CHAT_MESSAGES,
    NEW_TRADE_CHAT_MESSAGE,
    LOAD_TRADE_ADMIN_CHAT_MESSAGES,
    NEW_TRADE_ADMIN_CHAT_MESSAGE,
    ALERT
} from '../constants';
import { createApiClient } from './api';
import { loadExchangeAdvert } from './exchange';
import { getUserInfo } from './userInfo';
import { P2P_URL } from '../../config';

export function loadTrades() {
    return async function(dispatch, getState) {
        const trades = await createApiClient(getState).trades();

        dispatch({
            type: LOAD_TRADES,
            trades: trades.data
        });
    };
}

export function estimateTrade(advertId, amount, amountType, rate) {
    return async function(dispatch, getState) {
        dispatch({
            type: ESTIMATE_TRADE,
            amount,
            amountType,
            rate,
            estimate: await await createApiClient(getState).estimateTrade(advertId, amount, amountType, rate)
        });
    };
}

export function createTrade(advertId, amount, amountType, rate, details) {
    return async function(dispatch, getState) {
        try {
            const trade = await createApiClient(getState).createTrade(advertId, amount, amountType, rate, details);

            dispatch({
                type: LOAD_ONE_TRADE,
                id: trade.id,
                trade: trade
            });

            dispatch(push(`${P2P_URL}/trades/${trade.id}`));
        } catch (err) {
            if (err.response && err.response.status === 401) {
                dispatch({
                    type: ALERT,
                    message: {
                        type: 'error',
                        text: 'Для данного действия Вам необходимо войти в систему.'
                    }
                });
            } else if (err.response && err.response.status === 462) {
                dispatch({
                    type: ALERT,
                    message: {
                        type: 'warning',
                        text: 'Недостаточно стредств!'
                    }
                });
            } else if (err.response && err.response.status === 463) {
                await loadExchangeAdvert(advertId)(dispatch, getState);

                dispatch({
                    type: ALERT,
                    message: {
                        type: 'warning',
                        text: 'Владельцем объявления был изменён курс'
                    }
                });
            } else if (err.response && err.response.status == 472) {
                dispatch({
                    type: ALERT,
                    message: {
                        type: 'error',
                        text: 'create_trade_unavailable'
                    }
                });
            } else if (err.response && err.response.status === 476) {
                await loadExchangeAdvert(advertId)(dispatch, getState);

                dispatch({
                    type: ALERT,
                    message: {
                        type: 'error',
                        text: 'advert_unavailable'
                    }
                });
            } else {
                throw err;
            }
        }
    };
}

export function loadOneTrade(id) {
    return async function(dispatch, getState) {
        const trade = await createApiClient(getState).trade(id);
        const partner = await getUserInfo(trade.partner, trade.cryptocurrency.code, dispatch, getState);
        trade.partner = partner;

        dispatch({
            type: LOAD_ONE_TRADE,
            id: id,
            trade
        });
    };
}

export function tradeAction(id, action) {
    return async function(dispatch, getState) {
        try {
            await createApiClient(getState).tradeAction(id, action);
            await loadOneTrade(id)(dispatch, getState);
        } catch (err) {
            if (err.response && err.response.status === 466) {
                dispatch({
                    type: PROMPT_TRADE_DETAILS,
                    id
                });
            } else {
                throw err;
            }
        }
    };
}

export function cancelPromptTradeDetails(id) {
    return {
        type: CANCEL_PROMPT_TRADE_DETAILS,
        id: id
    };
}

export function tradeTimeout(id, timeout) {
    return async function(dispatch, getState) {
        await createApiClient(getState).tradeTimeout(id, timeout);
        await loadOneTrade(id)(dispatch, getState);
    };
}

export function tradeFeedback(id, code) {
    return async function(dispatch, getState) {
        await createApiClient(getState).tradeFeedback(id, code);
        await loadOneTrade(id)(dispatch, getState);
    };
}

export function describeDispute(id, description) {
    return async function(dispatch, getState) {
        await createApiClient(getState).describeDispute(id, description);
        await loadOneTrade(id)(dispatch, getState);
    };
}

export function setTradeDetails(id, details) {
    return async function(dispatch, getState) {
        await createApiClient(getState).setTradeDetails(id, details);
        dispatch(cancelPromptTradeDetails(id));
    };
}

export function loadTradeMessages(tradeId) {
    return async function(dispatch, getState) {
        dispatch({
            type: LOAD_TRADE_CHAT_MESSAGES,
            id: tradeId,
            messages: await createApiClient(getState).loadTradeMessages(tradeId)
        });
    };
}

export function sendTradeMessage(tradeId, message) {
    return async function(dispatch, getState) {
        dispatch({
            type: NEW_TRADE_CHAT_MESSAGE,
            id: tradeId,
            message: await createApiClient(getState).sendTradeMessage(tradeId, { message })
        });
    };
}

export function sendTradeFile(tradeId, file) {
    return async function(dispatch, getState) {
        dispatch({
            type: NEW_TRADE_CHAT_MESSAGE,
            id: tradeId,
            message: await createApiClient(getState).sendTradeFile(tradeId, file)
        });
    };
}

export function loadDisputeAdminMessages(tradeId) {
    return async function(dispatch, getState) {
        dispatch({
            type: LOAD_TRADE_ADMIN_CHAT_MESSAGES,
            id: tradeId,
            messages: await createApiClient(getState).loadTradeAdminMessages(tradeId)
        });
    };
}

export function sendDisputeAdminMessage(tradeId, message) {
    return async function(dispatch, getState) {
        dispatch({
            type: NEW_TRADE_ADMIN_CHAT_MESSAGE,
            id: tradeId,
            message: await createApiClient(getState).sendTradeAdminMessage(tradeId, { message })
        });
    };
}

export function sendDisputeAdminFile(tradeId, file) {
    return async function(dispatch, getState) {
        dispatch({
            type: NEW_TRADE_ADMIN_CHAT_MESSAGE,
            id: tradeId,
            message: await createApiClient(getState).sendTradeAdminFile(tradeId, file)
        });
    };
}
