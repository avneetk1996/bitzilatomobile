import { createApiClient } from './api';
import { LOAD_SIGNALS } from '../constants';

export function loadSignals(domain) {
    return async function(dispatch, getState) {
        const signals = await createApiClient(getState).loadSignals(domain);

        dispatch({
            type: LOAD_SIGNALS,
            signals,
            domain,
        });
    };
}
