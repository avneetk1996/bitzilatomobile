import _ from 'lodash';
import axios from 'axios';
import uuidv4 from 'uuid/v4';
import {AsyncStorage} from 'react-native'


const user_id=AsyncStorage.getItem('USERID');
const token=AsyncStorage.getItem('TOKEN');

export function createApiClient(getState) {
    const state = getState();
    return new BackendClient(
        _.get(state, '7'),
        _.get(state, 'ff'),
        _.get(state, 'backendUrl', ''),
        _.get(state, 'cookies')
    );
}

export class BackendClient {
    constructor(userId, token, backendUrl, cookies) {
        const headers = {};
     
        if (!_.isEmpty(cookies)) {
            headers['Cookie'] = _.map(cookies, (value, key) => `${key}=${value}`).join('; ');
        }

        this.publicAxios = axios.create({
            baseURL: `${backendUrl}/api/p2p`,
            headers
        });

        this.publicMarketAxios = axios.create({
            baseURL: `${backendUrl}/api/market/v1`,
            headers
        });

        if (userId && token) {
            console.log('USERID:::', result1)
            this.setUserToken(userId, token, backendUrl, headers);
        } else {
            this.axios = axios.create({
                baseURL: `${backendUrl}/api/p2p`,
                headers
            });

            this.marketAxios = axios.create({
                baseURL: `${backendUrl}/api/market/v1`,
                headers
            });
        }

        console.log("headers---",headers);
    }


    setUserToken(userId, token, backendUrl = '', headers) {
        this.userId = userId;
        this.axios = axios.create({
            baseURL: `${backendUrl}/api/p2p`,
            headers: {
                ...headers,
                Authorization: `Bearer ${token}`
            }
        });

        this.marketAxios = axios.create({
            baseURL: `${backendUrl}/api/market/v1`,
            headers: {
                ...headers,
                Authorization: `Bearer ${token}`
            }
        });

        this.publicMarketAxios = axios.create({
            baseURL: `${backendUrl}/api/market/v1`,
            headers: {
                ...headers,
                Authorization: `Bearer ${token}`
            }
        });

        this.authAxios = axios.create({
            baseURL: `${backendUrl}/api/auth`,
            headers: {
                ...headers,
                Authorization: `Bearer ${token}`
            }
        });
    }

    async sendFile(url, file) {
        const form = new FormData();
        form.append('file', file);
        if (file.type) {
            form.append('mime_type', file.type);
        }
        if (file.name) {
            form.append('name', file.name);
        }

        const r = await this.axios.post(url, form);
        return r.data;
    }

    isLogged() {
        return Boolean(this.userId);
    }

    async whoami(token) {
        const r = await this.publicAxios.get('/whoami', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return r.data;
    }

    ///////////////////////////////////////////////////////////////////////
    // stats
    ///////////////////////////////////////////////////////////////////////

    async lastDeal() {
        const r = await this.publicAxios.get('/public/stats/lastdeal');
        return {
            ...r.data,
            date: r.data.date && new Date(r.data.date)
        };
    }

    ///////////////////////////////////////////////////////////////////////
    // auth
    ///////////////////////////////////////////////////////////////////////

    async getAccessToken(regenerate) {
        const r = await this.authAxios.post('/access-token/', { regenerate });
        return r.data;
    }

    ///////////////////////////////////////////////////////////////////////
    // Profile
    ///////////////////////////////////////////////////////////////////////

    async profile() {
        const r = await this.axios.get(`/${this.userId}/profile/`);
        return formatProfile(r.data);
    }

    async updateProfile(opts) {
        const r = await this.axios.put(`/${this.userId}/profile/`, opts);
        return formatProfile(r.data);
    }

    async mergeProfile(telegram) {
        const r = await this.axios.post(`/${this.userId}/profile/merge`, {
            telegram
        });

        return formatProfile(r.data);
    }

    async referralLinks(cryptocurrency) {
        const r = await this.axios.get(
            `/${this.userId}/profile/referral-links`,
            cryptocurrency && {
                params: {
                    cryptocurrency
                }
            }
        );
        return r.data;
    }

    async getRateSource(cryptocurrency, currency) {
        const r = await this.axios.get(
            `/${this.userId}/profile/rate-sources/${cryptocurrency}`,
            currency && {
                params: { currency }
            }
        );

        return r.data;
    }

    async setRateSource(url, currency, cryptocurrency) {
        await this.axios.post(`/${this.userId}/profile/rate-sources/`, {
            url,
            currency,
            cryptocurrency
        });
    }

    async calcRate(cryptocurrency, currency, { value = null, percent = null }) {
        const r = await this.axios.post(`/${this.userId}/profile/rate-sources/${cryptocurrency}`, {
            currency,
            value,
            percent
        });

        return {
            value: _.toString(r.data.value),
            percent: _.toString(r.data.percent)
        };
    }

    async features() {
        if (this.isLogged()) {
            const r = await this.axios.get(`/${this.userId}/profile/features`);
            return r.data || [];
        } else {
            return [];
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // WALLET
    ///////////////////////////////////////////////////////////////////////

    async wallets() {
        const r = await this.axios.get(`/${this.userId}/wallets/`);
        return r.data;
    }

    async wallet(cryptocurrency, currency) {
        const r = await this.axios.get(`/${this.userId}/wallets/${cryptocurrency}`, {
            params: currency && { currency }
        });

        return formatWallet(r.data);
    }

    async walletStats(cryptocurrency, currency) {
        const r = await this.axios.get(`/${this.userId}/wallets/${cryptocurrency}/stats`, {
            params: { currency }
        });

        return {
            ...r.data,
            feedbacks: formatFeedbacks(r.data.feedbacks),
            totalCount: r.data.dealsCount,
            cryptocurrency
        };
    }

    async generateWalletAddress(cryptocurrency) {
        await this.axios.post(`/${this.userId}/wallets/generate-address`, {
            userId: this.userId,
            cryptocurrency: cryptocurrency
        });
    }

    async withdrawInfo(cryptocurrency) {
        const r = await this.axios.get(`/${this.userId}/wallets/${cryptocurrency}/withdrawal`);
        return r.data;
    }

    async withdraw(cryptocurrency, address, amount) {
        await this.axios.post(`/${this.userId}/wallets/${cryptocurrency}/withdrawal`, {
            address,
            amount
        });
    }

    ///////////////////////////////////////////////////////////////////////
    // Deep link
    ///////////////////////////////////////////////////////////////////////

    async deeplink(code, url) {
        const r = this.isLogged()
            ? await this.axios.post(`/${this.userId}/deeplink/`, { code, url })
            : await this.publicAxios.post('/public/deeplink/', { code, url });

        return {
            setCookies: r.headers['set-cookie'],
            ...r.data
        };
    }

    ///////////////////////////////////////////////////////////////////////
    // Vouchers
    ///////////////////////////////////////////////////////////////////////

    async vouchers({ cryptocurrency, limit, skip }) {
        const r = await this.axios.get(`/${this.userId}/vouchers/`, {
            params: {
                cryptocurrency,
                skip: skip || 0,
                limit: limit || 100
            }
        });

        const res = {
            data: _.map(r.data.data, formatVoucher),
            total: r.data.total
        };
        return res;
    }

    async createVoucher(amount, cryptocurrency, currency, method) {
        try {
            const params = { amount, cryptocurrency };

            if (currency) {
                params.currency = currency;
            }

            if (method) {
                params.method = method;
            } else {
                return false;
            }

            const r = await this.axios.post(`/${this.userId}/vouchers/`, params);

            return formatVoucher(r.data);
        } catch (err) {
            if (err.response && err.response.status === 463) {
                return false;
            } else {
                throw err;
            }
        }
    }

    async getVoucher(code) {
        const r = await this.axios.get(`/${this.userId}/vouchers/${code}`);
        return formatVoucher(r.data);
    }

    async cancelVoucher(code) {
        await this.axios.delete(`/${this.userId}/vouchers/${code}`);
    }

    ///////////////////////////////////////////////////////////////////////
    // Transactions
    ///////////////////////////////////////////////////////////////////////

    async transactions({ cryptocurrency, skip, limit }) {
        const r = await this.axios.get(`/${this.userId}/transactions/`, {
            params: {
                cryptocurrency,
                skip: skip || 0,
                limit: limit || 10
            }
        });
        return {
            data: _.map(r.data.data, formatTransaction),
            total: r.data.total
        };
    }

    async getTransaction(txid) {
        const r = await this.axios.get(`/${this.userId}/transactions/${txid}`);
        return formatTransaction(r.data);
    }

    async commentTransaction(txid, comment) {
        const r = await this.axios.put(`/${this.userId}/transactions/${txid}`, {
            comment
        });
        return formatTransaction(r.data);
    }

    ///////////////////////////////////////////////////////////////////////
    // Reports
    ///////////////////////////////////////////////////////////////////////

    async reports() {
        const r = await this.axios.get(`/${this.userId}/reports/`);

        return _.map(r.data, report => _.pick(report, ['code', 'description', 'format']));
    }

    async report(code, responseType = 'arraybuffer') {
        const r = await this.axios.get(`/${this.userId}/reports/${code}`, {
            responseType: responseType
        });

        return r.data;
    }

    ///////////////////////////////////////////////////////////////////////
    // Users
    ///////////////////////////////////////////////////////////////////////

    async userinfo(publicName) {
        const r = this.isLogged()
            ? await this.axios.get(`/${this.userId}/userinfo/${publicName}`)
            : await this.publicAxios.get(`/public/userinfo/${publicName}`);
        return formatUserInfo(r.data);
    }

    async loadUserMessages(publicName, { limit, before } = {}) {
        const r = await this.axios.get(`/${this.userId}/userinfo/${publicName}/chat/`, {
            limit: limit || 100,
            before: before || Date.now()
        });

        return _.map(r.data.data, item => formatChatMessage(item));
    }

    async sendUserMessage(publicName, { message }) {
        const r = await this.axios.post(`/${this.userId}/userinfo/${publicName}/chat/`, { message });
        return formatChatMessage(r.data);
    }

    async transferToUser(publicName, cryptocurrency, amount) {
        await this.axios.post(`/${this.userId}/userinfo/${publicName}/transfer`, {
            cryptocurrency,
            amount
        });
    }

    async blockUser(publicName, flag) {
        await this.axios.post(`/${this.userId}/userinfo/${publicName}/block`, {
            block: flag
        });
    }

    ///////////////////////////////////////////////////////////////////////
    // Adverts
    ///////////////////////////////////////////////////////////////////////

    async tradeStatus() {
        const r = await this.axios.get(`/${this.userId}/dsa/status`);
        return r.data;
    }

    async updateTradeStatus(cryptocurrency, flag) {
        await await this.axios.put(`/${this.userId}/dsa/status`, {
            [cryptocurrency]: Boolean(flag)
        });
    }

    async adverts(userId, token) {
        console.log('axios::', this.axios)
        const r = await this.axios.get(`/${userId}/dsa/all`, {
            headers: {
                Authorization: `Bearer ${token}`
            }});

        return Promise.all(_.map(r.data, advert => formatUserAdvert(this, advert)));
    }

    async createAdvert({
        type,
        cryptocurrency,
        paymethod,
        ratePercent,
        rateValue,
        minAmount,
        maxAmount,
        terms,
        details,
        userId,
        token
    }) {
        const data = {
            type,
            cryptocurrency,
            paymethod: _.parseInt(paymethod),
            minAmount,
            maxAmount,
            terms,
            details
        };

        if (ratePercent) {
            data.ratePercent = ratePercent;
        } else if (rateValue) {
            data.rateValue = rateValue;
        }

        const r = await this.axios.post(`/${userId}/dsa/`, data, {
            headers: {
                Authorization: `Bearer ${token}`
            }}
        );
        return  r.data;
    }

    async getAdvert(id) {
        const r = await this.axios.get(`/22/dsa/${id}`);
        return await formatUserAdvert(this, r.data);
    }

    async updateAdvert(id, { rate, limit, terms, details, status }) {
        const data = {};

        if (terms) {
            data.terms = terms;
        }

        if (details || details === null) {
            data.details = details;
        }

        if (status) {
            data.status = status;
        }

        if (limit) {
            data.minAmount = limit.min;
            data.maxAmount = limit.max;
        }

        if (rate && rate.percent) {
            data.ratePercent = rate.percent;
        } else if (rate && rate.value) {
            data.rateValue = rate.value;
        }

        await this.axios.put(`/${this.userId}/dsa/${id}`, data);
    }

    async removeAdvert(id) {
        await this.axios.delete(`/${this.userId}/dsa/${id}`);
    }

    async advertPaymethods({ type, currency, cryptocurrency, isOwnerActive, isOwnerVerificated }) {
        const r = await this.axios.get(
            this.isLogged() ? `/${this.userId}/exchange/paymethods/` : '/public/exchange/paymethods/',
            {
                params: _.pickBy({ type, currency, cryptocurrency, isOwnerActive, isOwnerVerificated })
            }
        );

        return _.map(r.data.data, ({ id, description }) => ({ id, name: description }));
    }

    async searchAdverts({
        type,
        cryptocurrency,
        paymethod = [],
        currency,
        isOwnerActive,
        isOwnerVerificated,
        limit = 10,
        skip = 0
    }) {
        const params = {
            type,
            cryptocurrency,
            paymethod,
            currency,
            limit,
            skip
        };

        params.paymethod = params.paymethod.join(',');

        if (isOwnerActive === true) {
            params.isOwnerActive = 'true';
        }

        if (isOwnerVerificated !== false) {
            params.isOwnerVerificated = 'true';
        }

        const url = this.isLogged() ? `${this.userId}/exchange/dsa/` : '/public/exchange/dsa/';

        const r = await this.axios.get(url, {
            params: _.pickBy(params)
        });

        return {
            data: _.map(r.data.data, item => ({
                ...item,
                ownerLastActivity: new Date(item.ownerLastActivity)
            })),
            total: r.data.total
        };
    }

    async getPublicAdvert(id) {
        const r = this.isLogged()
            ? await this.axios.get(`/${this.userId}/exchange/dsa/${id}`)
            : await this.publicAxios.get(`/public/exchange/dsa/${id}`);

        return formatAdvert(this, r.data);
    }

    ///////////////////////////////////////////////////////////////////////
    // Trades
    ///////////////////////////////////////////////////////////////////////

    async trades(skip = 0, limit = 10) {
        const r = await this.axios.get(`/${this.userId}/trade/`, {
            params: { limit, skip }
        });

        return {
            data: await Promise.all(
                _.map(r.data.data, async trade => ({
                    id: trade.id,
                    type: trade.type,
                    currency: trade.currency,
                    cryptocurrency: trade.cryptocurrency,
                    rate: trade.rate,
                    paymethod: await this.paymethod(trade.paymethod),
                    partner: trade.partner,
                    status: trade.status
                }))
            ),
            total: r.data.total
        };
    }

    async estimateTrade(advertId, amount, amountType, rate) {
        const r = await this.axios.post(`/${this.userId}/trade/idle`, {
            advertId,
            amount: amount.toString(),
            amountType,
            rate
        });

        return r.data;
    }

    async createTrade(advertId, amount, amountType, rate, details) {
        const r = await this.axios.post(`/${this.userId}/trade/`, {
            advertId,
            amount: amount.toString(),
            amountType,
            rate,
            counterDetails: details
        });

        return await formatTrade(this, r.data);
    }

    async trade(id) {
        try {
            const r = await this.axios.get(`/${this.userId}/trade/${id}`);
            return await formatTrade(this, r.data);
        } catch (err) {
            if (err.response && err.response.status === 404) {
                return null;
            } else {
                throw err;
            }
        }
    }

    async tradeAction(id, action) {
        await this.axios.post(`/${this.userId}/trade/${id}`, {
            type: action
        });
    }

    async tradeFeedback(id, code) {
        await this.axios.put(`/${this.userId}/trade/${id}/feedback`, { rate: code });
    }

    async describeDispute(id, description) {
        await this.axios.post(`/${this.userId}/trade/${id}/dispute/description`, { description });
    }

    async setTradeDetails(id, details) {
        await this.axios.put(`/${this.userId}/trade/${id}`, {
            details
        });
    }

    async tradeTimeout(id, timeout) {
        await this.axios.post(`/${this.userId}/trade/${id}/timeout`, {
            timeout
        });
    }

    async loadTradeMessages(tradeId, { limit, before } = {}) {
        const r = await this.axios.get(`/${this.userId}/trade/${tradeId}/chat/`, {
            limit: limit || 100,
            before: before || Date.now()
        });

        return _.map(r.data.data, formatChatMessage);
    }

    async sendTradeMessage(id, { message }) {
        const r = await this.axios.post(`/${this.userId}/trade/${id}/chat/`, { message });
        return formatChatMessage(r.data);
    }

    async sendTradeFile(tradeId, file) {
        return formatChatMessage(await this.sendFile(`/${this.userId}/trade/${tradeId}/chat/sendfile`, file));
    }

    async loadTradeAdminMessages(tradeId, { limit, before } = {}) {
        const r = await this.axios.get(`/${this.userId}/trade/${tradeId}/dispute/admin-chat/`, {
            limit: limit || 100,
            before: before || Date.now()
        });

        return _.map(r.data.data, formatChatMessage);
    }

    async sendTradeAdminMessage(id, { message }) {
        const r = await this.axios.post(`/${this.userId}/trade/${id}/dispute/admin-chat/`, { message });
        return formatChatMessage(r.data);
    }

    async sendTradeAdminFile(id, file) {
        return formatChatMessage(await this.sendFile(`/${this.userId}/trade/${id}/dispute/admin-chat/sendfile`, file));
    }

    ///////////////////////////////////////////////////////////////////////
    // notifications
    ///////////////////////////////////////////////////////////////////////

    getUniqueDeviceId(createIfNotExists) {
        if (typeof window !== 'undefined' && window.localStorage) {
            let deviceId = window.localStorage.getItem('uniqueDeviceId');

            if (!deviceId && createIfNotExists) {
                deviceId = uuidv4();
                window.localStorage.setItem('uniqueDeviceId', deviceId);
            }

            return deviceId;
        } else {
            throw new Error('Unsupported Browser');
        }
    }

    async setPushToken(token) {
        const deviceId = this.getUniqueDeviceId(true);
        await this.axios.post(`/${this.userId}/notifications/tokens/`, {
            deviceId,
            token
        });
    }

    async removePushToken() {
        const deviceId = this.getUniqueDeviceId();
        if (deviceId) {
            await this.axios.delete(`/${this.userId}/notifications/tokens/${deviceId}`);
        }
    }

    async notifications() {
        const r = await this.axios.get(`/${this.userId}/notifications/`);
        return _.map(r.data, formatNotification);
    }

    async notification(id) {
        const r = await this.axios.get(`/${this.userId}/notifications/${id}`);
        return formatNotification(r.data);
    }

    async markNotificationAsReaded(id) {
        await this.axios.delete(`/${this.userId}/notifications/${id}`);
    }

    ///////////////////////////////////////////////////////////////////////
    // refs
    ///////////////////////////////////////////////////////////////////////

    async currencies() {
        const r = await this.publicAxios.get('/public/refs/currencies');
        console.log("currencies response",r);
        return _.map(r.data, c => _.pick(c, ['code', 'name', 'sign']));
    }

    async cryptocurrencies() {
        const r = await this.publicAxios.get('/public/refs/cryptocurrencies');
        return _.map(r.data, c => _.pick(c, ['code', 'name']));
    }

    async paymethods(currency) {
        const r = await this.publicAxios.get('/public/refs/paymethods', {
            params: { currency }
        });

        return _.map(r.data, formatPaymethod);
    }

    async dsaPaymethods(type, currency, cryptocurrency, userId, token) {
        const r = await this.axios.get(`/${userId}/dsa/paymethods/${type}/${currency}/${cryptocurrency}/`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return r.data;
    }

    async paymethod(id) {
        const r = await this.publicAxios(`/public/refs/paymethods/${id}`);
        return formatPaymethod(r.data);
    }

    async feedbackRatings() {
        const r = await this.publicAxios.get('/public/refs/ratings');
        return r.data;
    }

    async rateSources({ cryptocurrency, currency }) {
        const r = await this.publicAxios.get('/public/refs/rate-sources', {
            params: {
                crypto: cryptocurrency,
                currency
            }
        });

        return r.data;
    }

    ///////////////////////////////////////////////////////////////////////
    // market
    ///////////////////////////////////////////////////////////////////////

    async loadPairs() {
        const r = await this.publicMarketAxios.get('/public/pairs/');
        return r.data;
    }

    async loadPair(pair) {
        const r = await this.publicMarketAxios.get(`/public/pairs/${pair}`);
        return r.data;
    }

    async loadOrders(pair, dir) {
        const r = await this.publicMarketAxios.get(`/public/orders/${pair}/${dir}`);
        return r.data;
    }

    async loadMarketTrades(pair) {
        const r = await this.publicMarketAxios.get(`/public/trades/${pair}/`);
        return r.data;
    }

    async createOrder(params) {
        await this.marketAxios.post(`/private/${this.userId}/orders/`, params);
    }

    async cancelOrder(id) {
        await this.marketAxios.delete(`/private/${this.userId}/orders/${id}`);
    }

    async loadMyOrders(params) {
        const r = await this.marketAxios.get(`/private/${this.userId}/orders/`, { params });
        return r.data;
    }

    async loadPersonalTrades(params) {
        const r = await this.marketAxios.get(`/private/${this.userId}/trades/`, { params });
        return r.data;
    }

    async loadCandlesticks(pair, params) {
        const r = await this.publicMarketAxios.get(`/public/stats/candlestick/${pair}/`, { params });
        return r.data;
    }

    ///////////////////////////////////////////////////////////////////////
    // signals
    ///////////////////////////////////////////////////////////////////////

    async loadSignals(domain) {
        const r = await this.axios.get(`/${this.userId}/alerts/${domain}`);
        return r.data;
    }
}

function formatProfile(profile) {
    return (
        profile && {
            name: profile.publicName,
            licensingAgreementAccepted: profile.licensingAgreementAccepted,
            lang: profile.lang,
            currency: profile.currency,
            totalAmount: profile.totalAmount,
            totalCount: profile.totalCount,
            cryptocurrency: profile.cryptocurrency,
            canCreateAdvert: profile.canCreateAdvert || {
                status: true
            },
            feedbacks: profile.feedbacks,
            serviceFee: profile.serviceFee,
            verification: profile.verification,
            greeting: profile.greeting,
            telegram: profile.telegram,
            lastActivity: profile.lastActivity && new Date(profile.lastActivity),
            startOfUseDate: new Date(profile.startOfUseDate),
            safeModeEnabled: profile.safeModeEnabled
        }
    );
}

function formatPaymethod(paymethod) {
    return {
        id: paymethod.id,
        currency: paymethod.currency,
        name: paymethod.description
    };
}

async function formatUserAdvert(store, advert) {
    const paymethod = await store.paymethod(advert.paymethod);

    return {
        id: advert.id,
        deepLinkCode: advert.deepLinkCode,
        type: advert.type,
        cryptocurrency: advert.cryptocurrency,
        paymethod: {
            id: paymethod.id,
            name: paymethod.name,
            currency: paymethod.currency
        },
        rate: {
            value: advert.rateValue,
            percent: advert.ratePercent
        },
        limit: {
            min: advert.minAmount,
            max: advert.maxAmount
        },
        terms: advert.terms,
        details: advert.details,
        status: advert.status,
        unactiveReason: advert.unactiveReason,
        owner: advert.owner,
        links: advert.links
    };
}

async function formatTrade(store, trade) {
    return {
        id: trade.id,
        type: trade.type,
        status: trade.status,
        paymethod: await store.paymethod(trade.currency.paymethod),
        currency: {
            code: trade.currency.code,
            amount: trade.currency.amount
        },
        cryptocurrency: {
            code: trade.cryptocurrency.code,
            amount: trade.cryptocurrency.amount
        },
        history: formatHistory(trade.history),
        rate: trade.rate,
        terms: trade.terms,
        details: trade.details,
        counterDetails: trade.counterDetails,
        fee: trade.fee,
        availableActions: trade.availableActions,
        partner: trade.partner,
        times: _.transform(trade.times, (result, value, key) => _.set(result, key, value && new Date(value)), {})
    };
}

function formatUserInfo(user = {}) {
    const info = {
        name: user.name,
        greeting: user.greeting,
        startOfUseDate: new Date(user.startOfUseDate),
        totalAmount: user.totalAmount,
        totalCount: user.totalCount,
        activeDeals: user.activeDeals,
        canceledDeals: user.canceledDeals,
        defeatInDisputes: user.defeatInDisputes,
        verification: user.verification,
        rating: user.rating,
        feedbacks: formatFeedbacks(user.feedbacks),
        chatAvailable: Boolean(user.chatAvailable),
        dealStats: user.dealStats,
        suspicious: user.suspicious
    };

    if (_.has(user, 'lastActivity')) {
        info.lastActivity = new Date(user.lastActivity);
    }

    if (_.has(user, 'blocked')) {
        info.blocked = user.blocked;
    }

    return info;
}

function formatFeedbacks(feedbacks) {
    let thumbUp = 0,
        thumbDown = 0;

    _.forEach(feedbacks, item => {
        if (item.type === 'thumb-up' || item.type === 'relieved') {
            thumbUp += item.count;
        } else {
            thumbDown += item.count;
        }
    });

    return { thumbUp, thumbDown };
}

function formatHistory(history) {
    return _.map(history, hist => ({
        date: new Date(hist.date),
        status: hist.status
    }));
}

async function formatAdvert(api, advert) {
    return {
        id: advert.id,
        deepLinkCode: advert.deepLinkCode,
        type: advert.type,
        cryptocurrency: advert.cryptocurrency,
        paymethod: await api.paymethod(advert.paymethod),
        rate: advert.rate,
        limit: {
            min: advert.min,
            max: advert.max,
            sellingMax: advert.sellingMax
        },
        terms: advert.terms,
        owner: advert.owner,
        status: advert.status,
        available: advert.available
    };
}

function formatWallet(wallet) {
    return (
        wallet && {
            ...wallet,
            createdAt: new Date(wallet.createdAt)
        }
    );
}

function formatVoucher(voucher) {
    return (
        voucher && {
            deepLinkCode: voucher.deepLinkCode,
            currency: voucher.currency,
            cryptocurrency: voucher.cryptocurrency,
            links: voucher.links,
            createdAt: new Date(voucher.createdAt)
        }
    );
}

function formatTransaction(tx) {
    return (
        tx && {
            id: tx.id,
            type: tx.type,
            created: new Date(tx.created),
            cryptocurrency: {
                code: tx.cryptocurrency.code,
                amount: tx.cryptocurrency.amount
            },
            address: tx.address,
            comment: tx.comment,
            viewUrl: tx.viewUrl
        }
    );
}

function formatChatMessage(msg) {
    return (
        msg && {
            ...msg,
            created: new Date(msg.created)
        }
    );
}

function formatNotification(notification) {
    return {
        id: notification.id,
        name: notification.name,
        data: notification.data
    };
}
