import { OPEN_ACCESS_TOKEN, CLOSE_ACCESS_TOKEN, GET_ACCESS_TOKEN, LOAD_USERID } from '../constants';
import { createApiClient } from './api';

export function openAccessToken() {
    return {
        type: OPEN_ACCESS_TOKEN
    };
}

export function closeAccessToken() {
    return {
        type: CLOSE_ACCESS_TOKEN
    };
}

export function getAccessToken(regenerate = false ) {
    return async function(dispatch, getState) {
        const data = await createApiClient(getState).getAccessToken(regenerate);

        dispatch({
            type: GET_ACCESS_TOKEN,
            data
        });
    };
}

export function whoami(token) {
    return async function(dispatch, getState) {
        const whoami = await createApiClient(getState).whoami(token);

        dispatch({
            type: LOAD_USERID,
            whoami
        });
    };
}