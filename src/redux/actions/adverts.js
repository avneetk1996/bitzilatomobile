import _ from 'lodash';
import { push } from 'connected-react-router';
import { createApiClient } from './api';
import { LOAD_ADVERTS, LOAD_ONE_ADVERT, REMOVE_ADVERT, LOAD_PAYMETHODS, LOAD_TRADE_STATUS, ALERT } from '../constants';
import { addSnackbar } from './snackbars';

import { P2P_URL } from '../../config';

export function loadAdverts(userId, token) {
    return async function(dispatch, getState) {
        const adverts = await createApiClient(getState).adverts(userId, token);

        dispatch({
            type: LOAD_ADVERTS,
            adverts: adverts
        });
    };
}

export function loadOneAdvert(id) {
    return async function(dispatch, getState) {
        dispatch({
            type: LOAD_ONE_ADVERT,
            id: id,
            advert: await createApiClient(getState).getAdvert(id)
        });
    };
}

export function updateAdvert(id, { rate, limit, terms, details, status }) {
    return async function(dispatch, getState) {
        await createApiClient(getState).updateAdvert(id, { rate, limit, terms, details, status });
        await loadOneAdvert(id)(dispatch, getState);
    };
}

export function removeAdvert(id) {
    return async function(dispatch, getState) {
        await createApiClient(getState).removeAdvert(id);

        dispatch({
            type: REMOVE_ADVERT,
            id: id
        });

        dispatch(push(`${P2P_URL}/adverts/`));

        dispatch(addSnackbar('Объявление успешно удалено!'));
    };
}

export function createAdvert({
    type,
    cryptocurrency,
    paymethod,
    rateValue,
    ratePercent,
    minAmount,
    maxAmount,
    terms,
    details,
    userId,
        token
}) {
    console.log('received')
    return async function(dispatch, getState) {
        try {
            const opts = {
                type,
                cryptocurrency,
                paymethod,
                rateValue,
                ratePercent,
                minAmount,
                maxAmount,
                terms,
                details,
                userId,
                token
            };
            const advert = await createApiClient(getState).createAdvert(opts);

            dispatch({
                type: LOAD_ONE_ADVERT,
                id: advert.id,
                advert: advert
            });

            await loadAdverts()(dispatch, getState);

            dispatch(push(`${P2P_URL}/adverts/${advert.id}`));
        } catch (err) {
            if (err.response && err.response.status === 468) {
                dispatch({
                    type: ALERT,
                    message: {
                        type: 'error',
                        text: 'not_enough_rating_to_create_advert'
                    }
                });
            } else {
                throw err;
            }
        }
    };
}

export function loadAdvertPaymethods({ type, cryptocurrency, currency, isOwnerActive, isOwnerVerificated }) {
    return async function(dispatch, getState) {
        const paymethods = await createApiClient(getState).advertPaymethods({
            type,
            cryptocurrency,
            currency,
            isOwnerActive,
            isOwnerVerificated
        });

        dispatch({
            type: LOAD_PAYMETHODS,
            advertType: type,
            currency: _.join([type, cryptocurrency, currency, isOwnerActive, isOwnerVerificated], '-'),
            paymethods
        });
    };
}

export function loadTradeStatus() {
    return async function(dispatch, getState) {
        dispatch({
            type: LOAD_TRADE_STATUS,
            tradeStatus: await createApiClient(getState).tradeStatus()
        });
    };
}

export function updateTradeStatus(cryptocurrency, flag) {
    return async function(dispatch, getState) {
        await createApiClient(getState).updateTradeStatus(cryptocurrency, flag);
        await loadTradeStatus()(dispatch, getState);
    };
}
