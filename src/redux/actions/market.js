import { change, getFormValues } from 'redux-form';
import moment from 'moment-timezone';

import { createApiClient } from './api';
import { addSnackbar } from './snackbars';
import { loadAllWalletsInfo } from './wallets';
import {
    ALERT,
    LIST_PAIRS,
    LOAD_PAIR,
    LOAD_ORDERBOOK,
    LOAD_PERSONAL_ORDERS,
    LOAD_MARKET_TRADES,
    LOAD_MY_ORDERS,
    SWITCH_PAIR,
    EVENT_NEW_ORDER,
    EVENT_NEW_ORDER_PERSONAL,
    EVENT_ORDER_REMOVED,
    EVENT_ORDER_REMOVED_PERSONAL,
    EVENT_ORDER_CHANGED,
    EVENT_ORDER_CHANGED_PERSONAL,
    EVENT_NEW_TRADES,
    LOAD_CANDLESTICK_START,
    LOAD_CANDLESTICK_END,
    SET_MYORDERS_FILTER,
    SET_MYORDERS_PAGE,
    SET_MYORDERS_LIMIT,
    LOAD_PERSONAL_TRADES,
    SET_PERSONAL_TRADES_FILTER,
    SET_PERSONAL_TRADES_PAGE,
    SET_PERSONAL_TRADES_LIMIT
} from '../constants';

import { round10, isNumberComma, strToNumComma } from '../../common/math';

export function switchPair(pair) {
    return {
        type: SWITCH_PAIR,
        pair
    };
}

export function loadPairsList() {
    return async function(dispatch, getState) {
        const pairs = await createApiClient(getState).loadPairs();

        dispatch({
            type: LIST_PAIRS,
            pairs
        });
    };
}

export function loadPair(pair) {
    return async function(dispatch, getState) {
        const data = await createApiClient(getState).loadPair(pair);

        await dispatch({
            type: LOAD_PAIR,
            pair,
            data
        });
    };
}

// The bid price is what buyers are willing to pay for it.
// The ask price is what sellers are willing to take for it.
export function loadOrderbook(pair) {
    return async function(dispatch, getState) {
        const [{ data: bid }, { data: ask }] = await Promise.all([
            createApiClient(getState).loadOrders(pair, 'bid'),
            createApiClient(getState).loadOrders(pair, 'ask')
        ]);

        dispatch({
            type: LOAD_ORDERBOOK,
            pair,
            dir: 'bid',
            orderbook: bid
        });
        dispatch({
            type: LOAD_ORDERBOOK,
            pair,
            dir: 'ask',
            orderbook: ask
        });
    };
}

export function createOrder(params) {
    return async function(dispatch, getState) {
        try {
            await createApiClient(getState).createOrder(params);
        } catch (err) {
            if (err.response && err.response.status === 401) {
                dispatch({
                    type: ALERT,
                    message: {
                        type: 'error',
                        text: 'Для совершения сделки Вам необходимо войти в систему'
                    }
                });
            } else {
                throw err;
            }
        }
    };
}

export function loadPersonalOrders(pair) {
    return async function(dispatch, getState) {
        const { data, total } = await createApiClient(getState).loadMyOrders({ pair });

        dispatch({
            type: LOAD_PERSONAL_ORDERS,
            pair,
            orders: data,
            total
        });
    };
}

export function cancelOrder(id) {
    return async function(dispatch, getState) {
        await createApiClient(getState).cancelOrder(id);
    };
}

export function loadMarketTrades(pair) {
    return async function(dispatch, getState) {
        const trades = await createApiClient(getState).loadMarketTrades(pair);
        dispatch({
            type: LOAD_MARKET_TRADES,
            pair,
            trades: trades.data
        });
    };
}

export function loadMyOrders() {
    return async function(dispatch, getState) {
        const { filter, limit, skip } = getState().market.myorders;
        const { data, total } = await createApiClient(getState).loadMyOrders({ ...filter, limit, skip });

        dispatch({
            type: LOAD_MY_ORDERS,
            orders: data,
            total
        });
    };
}

export function setMyOrdersFilter(filter = {}) {
    return {
        type: SET_MYORDERS_FILTER,
        filter
    };
}

export function myOrdersChangePage(page) {
    return {
        type: SET_MYORDERS_PAGE,
        page
    };
}

export function myOrdersChangeLimit(limit) {
    return {
        type: SET_MYORDERS_LIMIT,
        limit
    };
}

export function eventNewOrder(event) {
    return {
        type: EVENT_NEW_ORDER,
        event
    };
}

export function eventNewOrderPersonal(event) {
    return async function(dispatch, getState) {
        dispatch({ type: EVENT_NEW_ORDER_PERSONAL, event });

        const [base, quote] = event.pair.split('-');
        const context = { ...event, base, quote };
        const strId = event.data.offerType === 'bid' ? 'eventNewOrderBid' : 'eventNewOrderAsk';
        dispatch(addSnackbar(strId, context));
    };
}

export function orderRemoved(event) {
    return {
        type: EVENT_ORDER_REMOVED,
        event
    };
}

export function orderRemovedPersonal(event) {
    return async function(dispatch, getState) {
        dispatch({ type: EVENT_ORDER_REMOVED_PERSONAL, event });

        const [base, quote] = event.pair.split('-');
        const context = { ...event, base, quote };
        let strId;
        if (event.data.reason === 'Order was completed') {
            // TODO сделаем когда будут нужные данные
            strId = event.data.offerType === 'bid' ? 'orderRemovedCompletedBid' : 'orderRemovedCompletedAsk';
            return;
        } else {
            strId = event.data.offerType === 'bid' ? 'orderRemovedCancelledBid' : 'orderRemovedCancelledAsk';
        }
        dispatch(addSnackbar(strId, context));
    };
}

export function orderChanged(event) {
    return {
        type: EVENT_ORDER_CHANGED,
        event
    };
}

export function orderChangedPersonal(event) {
    return async function(dispatch, getState) {
        dispatch({ type: EVENT_ORDER_CHANGED_PERSONAL, event });

        const [base, quote] = event.pair.split('-');
        const context = { ...event, base, quote };
        const strId = event.data.offerType === 'bid' ? 'orderChangedBid' : 'orderChangedAsk';
        dispatch(addSnackbar(strId, context));
    };
}

export function eventNewTrades(event) {
    return {
        type: EVENT_NEW_TRADES,
        event
    };
}

export function setAmount(dir, amount) {
    return async function(dispatch, getState) {
        const { price } = getFormValues(dir)(getState());
        dispatch(change(dir, 'amount', String(amount)));
        if (isNumberComma(price)) {
            dispatch(change(dir, 'total', String(round10(strToNumComma(price) * strToNumComma(amount)))));
        } else {
            dispatch(change(dir, 'total', ''));
        }
    };
}

export function setPrice(dir, price) {
    return async function(dispatch, getState) {
        const { amount } = getFormValues(dir)(getState());
        dispatch(change(dir, 'price', String(price)));
        if (isNumberComma(amount)) {
            dispatch(change(dir, 'total', String(round10(strToNumComma(amount) * price))));
        } else {
            dispatch(change(dir, 'total', ''));
        }
    };
}

export function setTotal(dir, total) {
    return async function(dispatch, getState) {
        const { price, amount } = getFormValues(dir)(getState());
        dispatch(change(dir, 'total', String(total)));
        if (isNumberComma(price)) {
            dispatch(change(dir, 'amount', String(round10(strToNumComma(total) / strToNumComma(price)))));
        } else if (isNumberComma(amount)) {
            dispatch(change(dir, 'price', String(round10(strToNumComma(total) / strToNumComma(amount)))));
        }
    };
}

export function setAmountAndPrice(dir, amount, price) {
    return async function(dispatch) {
        dispatch(change(dir, 'amount', String(amount)));
        dispatch(change(dir, 'price', String(price)));
        dispatch(change(dir, 'total', String(round10(amount * price))));
    };
}

const LIMIT = 100;

const widthToMs = width => {
    switch (width) {
        case '1m':
            return 60 * 1000;
        case '10m':
            return 60 * 10 * 1000;
        case '1h':
            return 60 * 60 * 1000;
        case '1d':
            return 24 * 60 * 60 * 1000;
    }
};

const endOf = (ms, width) => {
    ms = ms || moment().valueOf();
    // prettier-ignore
    switch (width) {
        case '1m':
            return moment(ms).utc().endOf('minute').valueOf() + 1;
        case '1h':
            return moment(ms).utc().endOf('hour').valueOf() + 1;
        case '1d':
            return moment(ms).utc().endOf('day').valueOf() + 1;
        case '10m': {
            const widthMs = widthToMs(width);
            let time = moment(ms).utc().endOf('hour').valueOf() + 1;
            while (time - ms > widthMs) time -= widthMs;
            return time;
        }
    }
};

const startOf = (ms, width) => {
    ms = ms || moment().valueOf();
    // prettier-ignore
    switch (width) {
        case '1m':
            return moment(ms).utc().startOf('minute').valueOf();
        case '1h':
            return moment(ms).utc().startOf('hour').valueOf();
        case '1d':
            return moment(ms).utc().startOf('day').valueOf();
        case '10m': {
            const widthMs = widthToMs(width);
            let time = moment(ms).utc().startOf('hour').valueOf();
            while (ms - time > widthMs) time += widthMs;
            return time;
        }
    }
};

export function loadCandlestick(pair, before, after, width) {
    const widthMs = widthToMs(width);
    if (!before) before = endOf(null, width);
    if (!after) after = before - widthMs * (LIMIT - 3);

    return async function(dispatch, getState) {
        const keysToFetchAll = [];
        const start = startOf(after, width);
        for (let i = start; i <= before; i += widthMs) {
            keysToFetchAll.push(i);
        }

        const chunks = [];
        while (keysToFetchAll.length > 0) chunks.push(keysToFetchAll.splice(0, LIMIT));
        chunks.reverse();

        for (const keysToFetch of chunks) {
            dispatch({
                type: LOAD_CANDLESTICK_START,
                params: { pair, width, keysToFetch }
            });

            const after = keysToFetch[0];
            const before = keysToFetch[keysToFetch.length - 1];

            const res = await createApiClient(getState).loadCandlesticks(pair, { after, before, width });
            const data = res.map(({ date, price: { open, high, low, close }, volume: { base } }) =>
                close == 0 && base == 0
                    ? { date }
                    : {
                          date,
                          open,
                          high,
                          low,
                          close,
                          volume: base
                      }
            );

            dispatch({
                type: LOAD_CANDLESTICK_END,
                params: { pair, width, keysToFetch },
                data
            });
        }
    };
}

export function downloadMoreCandlestick(pair, start, end, width) {
    const widthMs = widthToMs(width);
    return async function(dispatch, getState) {
        const items = Math.floor(end - start);
        if (items === 0) return;
        const key = `${pair}-${width}`;
        const first = getState().market.candlestick[key][0].date;

        const before = first - widthMs;
        const after = first - widthMs * items;

        dispatch(loadCandlestick(pair, before, after, width));
    };
}

export function loadNewCandles(pair, width) {
    const widthMs = widthToMs(width);
    return async function(dispatch, getState) {
        const key = `${pair}-${width}`;
        const arr = getState().market.candlestick[key];
        if (!arr) return;
        const after = arr[arr.length - 1].date - widthMs;
        const before = endOf(null, width);

        dispatch(loadCandlestick(pair, before, after, width));
    };
}

export function loadPersonalTrades() {
    return async function(dispatch, getState) {
        const { filter, limit, skip } = getState().market.personalTrades;
        const { data, total } = await createApiClient(getState).loadPersonalTrades({ ...filter, limit, skip });

        dispatch({
            type: LOAD_PERSONAL_TRADES,
            trades: data,
            total
        });
    };
}

export function personalTradesFilter(filter = {}) {
    return {
        type: SET_PERSONAL_TRADES_FILTER,
        filter
    };
}

export function personalTradesChangePage(page) {
    return {
        type: SET_PERSONAL_TRADES_PAGE,
        page
    };
}

export function personalTradesChangeLimit(limit) {
    return {
        type: SET_PERSONAL_TRADES_LIMIT,
        limit
    };
}

export function updateBalanceForPair(pair) {
    return async function(dispatch, getState) {
        const wallets = pair.split('-');
        await dispatch(loadAllWalletsInfo(wallets));
    };
}
