import React, { Component } from 'react';
import {
    reduxifyNavigator,
    createReactNavigationReduxMiddleware,
    createNavigationReducer
} from 'react-navigation-redux-helpers';
import {connect}from "react-redux";
import Home from '../screens/home';
import Login from '../screens/login';
import Header from '../common/Header';
import AddAnnouncement from '../screens/announcement/AddAnnouncement';
import Announcements from '../screens/announcement/Announcements';

import { createStackNavigator, createAppContainer } from 'react-navigation';

const HomeStack = createStackNavigator(
    {
        Home: { screen: Home, navigationOptions: { header: <Header /> } },
        AddAnnouncement: {
            screen: AddAnnouncement,
            navigationOptions: { header: <Header /> }
        },
        Announcements: {
            screen: Announcements,
            navigationOptions: { header: <Header /> }
        }
    },
    {
        initialRouteName: 'Home'
    }
);

const Navigator = createStackNavigator(
    {
        Dashboard: { screen: HomeStack },
        Login: { screen: Login }
    },
    {
        initialRouteName: 'Dashboard',
        headerMode: 'none'
    }
);

export const navReducer = createNavigationReducer(Navigator);
export const navigationMiddleware = createReactNavigationReduxMiddleware('root', state => state.nav);

const ReduxNavigator = reduxifyNavigator(Navigator, 'root');
const mapStateToProps = state => ({
    state: state.navReducer
});

export default connect(mapStateToProps)(ReduxNavigator);
